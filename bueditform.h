#ifndef BUEDITFORM_H
#define BUEDITFORM_H

#include <string>
#include "SDL2/SDL.h"
#include "charset.h"

using namespace std;

class BUEditform
{
public:
    BUEditform();

    static void show(string &_title, string &_sub_title, string &_var_content, string &_ok_btn, string &_cancel_btn);
    static bool is_selected();
    static int get_result();
    static string get_text();


    static void draw(SDL_Renderer* r);
    static void events(SDL_Event &e);

private:
    static string title;
    static string sub_title;

    static string ok_btn;
    static string cancel_btn;

    static bool selected;
    static bool active;
    static string text;
    static int result;
};

#endif // BUEDITFORM_H
