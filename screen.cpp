#include "screen.h"
#include "g.h"
#include "basic_render.h"

vector<shared_ptr<Screen::Command>> Screen::commands;
vector<shared_ptr<Screen::Gel_to_load>> Screen::gel_load_queue;
map<string,shared_ptr<Screen::Gel>> Screen::gells;
map<string,shared_ptr<Screen::Sprite>> Screen::sprites;

mutex Screen::com_mutex;

SDL_Texture *Screen::draw_buffer;

bool Screen::clear_flag=false;

Screen::Screen()
{

}

void Screen::draw(SDL_Renderer *r)
{

    SDL_SetRenderTarget(r, draw_buffer);

    //loading gels from queue

    while(gel_load_queue.size()>0){
        shared_ptr<Gel> g (new Gel);
        g->name=gel_load_queue.at(0)->name;
        g->texture.load(gel_load_queue.at(0)->file,r);
        g->w=g->texture.get_w();
        g->h=g->texture.get_h();
        gells.insert(std::pair<string,shared_ptr<Gel>>(g->name,g) );
        //delete gel_load_queue.at(0);
        gel_load_queue.erase(gel_load_queue.begin());
    }


    //drawing queue
    com_mutex.lock();

    Uint8 red,green,blue,alpha;
    SDL_GetRenderDrawColor(r,&red,&green,&blue,&alpha);

    for(size_t i=0;i<commands.size();i++){
        shared_ptr<Command> com = commands.at(i);
        if(com)
            com->run(r);
    }

    SDL_SetRenderTarget(r, nullptr);
    SDL_RenderCopy(r,draw_buffer,nullptr,nullptr);


    //drawing sprites
    for (auto &e: sprites) {
        string gelname = e.second->gelname;
        if(gells.find(gelname) == gells.end()){
            std_debug("No gel",gelname);
            continue;
        }
        gells.at(gelname)->texture.draw(e.second->x,e.second->y);
    }

    SDL_SetRenderDrawColor(r,red,green,blue,alpha);

    //deleting
    //std::shared_ptr<Command> com;

    if(commands.size()>1000){
        /*com =commands.at(0);
        std_debug("trying to delete ",com.get());*/
        //commands.erase(commands.begin(),commands.begin()+commands.size()-1000);
        commands.clear();
    }
    com_mutex.unlock();
}

void Screen::clear()
{
    clear_commands();

    gel_load_queue.clear();


    gells.clear();


    sprites.clear();
}

void Screen::clear_commands()
{

    commands.clear();
}

void Screen::set_clear_flag(bool val)
{
    clear_flag=val;
}

bool Screen::get_clear_flag()
{
    return clear_flag;
}

void Screen::init(SDL_Renderer* r)
{
    draw_buffer = SDL_CreateTexture(r, SDL_PIXELFORMAT_RGBA8888,SDL_TEXTUREACCESS_TARGET, G::scr_w, G::scr_h);
    //draw_buffer = SDL_CreateTexture(r, SDL_PIXELFORMAT_RGBA8888,SDL_TEXTUREACCESS_TARGET, 176, 220);
    bas_cls();
}

void Screen::free()
{
    SDL_DestroyTexture(draw_buffer);
}

void Screen::bas_gelload(string name, string filename)
{
    shared_ptr<Gel_to_load> g(new Gel_to_load);
    g->name=name;
    g->file=filename;
    gel_load_queue.push_back(g);
}

bool Screen::bas_spritehit(string sprite_name1, string sprite_name2)
{
    if(sprites.find(sprite_name1) == sprites.end()){
        std_debug("sprite not found",sprite_name1);
        return false;
    }
    if(sprites.find(sprite_name2) == sprites.end()){
        std_debug("sprite not found",sprite_name1);
        return false;
    }
    shared_ptr<Sprite> s1 = sprites.at(sprite_name1);
    shared_ptr<Sprite> s2 = sprites.at(sprite_name2);

    int x1=s1->x;
    int y1=s1->y;

    if(gells.find(s1->gelname) == gells.end()){
        std_debug("Gel not found",s1->gelname);
        return false;
    }

    int w1=gells.at(s1->gelname)->w;
    int h1=gells.at(s1->gelname)->h;

    int x2=s2->x;
    int y2=s2->y;

    if(gells.find(s2->gelname) == gells.end()){
        std_debug("Gel not found",s2->gelname);
        return false;
    }

    int w2=gells.at(s2->gelname)->w;
    int h2=gells.at(s2->gelname)->h;

    if((x1>x2)&&(x1<x2+w2)){
        if((y1>y2)&&(y1<y2+h2)){
            return true;
        }
        if((y2>y1)&&(y2<y1+h1)){
            return true;
        }
    }
    if((x2>x1)&&(x2<x1+w1)){
        if((y1>y2)&&(y1<y2+h2)){
            return true;
        }
        if((y2>y1)&&(y2<y1+h1)){
            return true;
        }
    }

    return false;
}


void Screen::bas_set_color(char r, char g, char b)
{
    shared_ptr<Setcolor> com(new Setcolor());

    com->params.push_back(r);
    com->params.push_back(g);
    com->params.push_back(b);
    append(com);
}

void Screen::bas_cls()
{
    clear_commands();
    shared_ptr<Clear> com(new Clear());
    append(com);
}

void Screen::bas_fillrect(int x, int y, int w, int h)
{
    shared_ptr<Fillrect> com(new Fillrect());

    com->params.push_back(x);
    com->params.push_back(y);
    com->params.push_back(w);
    com->params.push_back(h);
    append(com);
}

void Screen::bas_drawstring(string str, int x, int y)
{
    shared_ptr<Drawstring> com(new Drawstring());

    com->str = str;
    com->params.push_back(x);
    com->params.push_back(y);

    append(com);
}

void Screen::bas_drawgel(string name, int x, int y)
{
    shared_ptr<Drawgel> com(new Drawgel());

    com->gelname = name;
    com->x = x;
    com->y = y;

    append(com);
}

void Screen::bas_plot(int x, int y)
{
    shared_ptr<Plot> com(new Plot());
    com->x=x;
    com->y=y;
    append(com);
}

void Screen::bas_spritegel(string sprite_name, string gel_name)
{
    /*if(gells.find(gel_name) == gells.end()){
        std_debug("Gel not exist!",gel_name);
        return;
    }*/

    if(sprites.find(sprite_name)==sprites.end()){
        shared_ptr<Sprite> s(new Sprite);

        s->x=0;
        s->y=0;
        s->gelname=gel_name;

        sprites.insert({sprite_name,s});
    }else
        sprites.at(sprite_name)->gelname=gel_name;
    //std_debug("sprite created!");

    Basic_render::console_mode=false;
}

void Screen::bas_spritemove(string sprite_name, int x, int y)
{
    shared_ptr<Sprite> spr;
    spr = sprites.at(sprite_name);
    spr->x=x;
    spr->y=y;

    Basic_render::console_mode=false;
}



void Screen::append(shared_ptr<Screen::Command> com)
{
    com_mutex.lock();
    commands.push_back(com);

    Basic_render::console_mode=false;
    com_mutex.unlock();
}

void Screen::Setcolor::run(SDL_Renderer *r)
{

    SDL_SetRenderDrawColor(r,params.at(0),params.at(1),params.at(2),0xff);
    Basic_render::font.set_color({(Uint8)params.at(0),(Uint8)params.at(1),(Uint8)params.at(2)});
}

void Screen::Clear::run(SDL_Renderer *r)
{
    Uint8 red,green,blue,alpha;
    SDL_GetRenderDrawColor(r,&red,&green,&blue,&alpha);
    SDL_SetRenderDrawColor(r,0xff,0xff,0xff,0xff);
    SDL_RenderFillRect(r,nullptr);
    SDL_SetRenderDrawColor(r,red,green,blue,alpha);
}

void Screen::Fillrect::run(SDL_Renderer *r)
{
    SDL_Rect rect;
    rect.x = params.at(0);
    rect.y = params.at(1);
    rect.w = params.at(2);
    rect.h = params.at(3);

    SDL_RenderFillRect(r,&rect);
}

void Screen::Drawstring::run(SDL_Renderer *r)
{
    Basic_render::font.drawText(str,params.at(0),params.at(1));
}

void Screen::Drawgel::run(SDL_Renderer *r)
{
    if(gells.find(gelname) == gells.end()){
        //std_debug("gel",gelname,"not found!");
        return;
    }
    auto g = gells.at(gelname);
    g->texture.draw(x,y);

}

void Screen::Plot::run(SDL_Renderer *r)
{
    SDL_RenderDrawPoint(r,x,y);
}
