#ifndef SCREEN_H
#define SCREEN_H

#include <vector>
#include <map>
#include <memory>
#include <SDL2/SDL.h>
#include <mutex>


#include "g.h"
#include "texture.h"

using namespace std;

class Screen
{
    class Command{
    public:
        virtual ~Command(){
            //std_debug("command delete",hex,this);
        };
        virtual void run(SDL_Renderer *r){};
    };

    class Setcolor : public Command{
    public:
        void run(SDL_Renderer *r);
        vector<char> params;
    };

    class Clear : public Command{
    public:
        void run(SDL_Renderer *r);
    };

    class Fillrect : public Command{
    public:
        void run(SDL_Renderer *r);
        vector<int> params;
    };

    class Drawstring : public Command{
    public:
        void run(SDL_Renderer *r);
        string str;
        vector<int> params;
    };

    class Drawgel : public Command{
    public:
        void run(SDL_Renderer *r);
        string gelname;
        int x,y;
    };

    class Plot : public Command{
    public:
        void run(SDL_Renderer *r);
        int x,y;
    };

//-----------------------------

    struct Gel{
        string name;
        Texture texture;
        int w;
        int h;
    };

    struct Gel_to_load{
        string name;
        string file;
    };

    struct Sprite{
        string gelname;
        int x;
        int y;
    };

public:
    Screen();
    static void draw(SDL_Renderer* r);
    static void clear();
    static void clear_commands();
    static void set_clear_flag(bool val);
    static bool get_clear_flag();
    static void init(SDL_Renderer* r);
    static void free();

    static void bas_gelload(string name, string filename);

    static bool bas_spritehit(string sprite_name1, string sprite_name2);

    static void bas_spritegel(string sprite_name, string gel_name);
    static void bas_spritemove(string sprite_name, int x, int y);

    static void bas_set_color(char r, char g, char b);
    static void bas_cls();
    static void bas_fillrect(int x, int y, int w, int h);
    static void bas_drawstring(string str, int x, int y);
    static void bas_drawgel(string name, int x, int y);
    static void bas_plot(int x, int y);

private:
    static vector<shared_ptr<Command>> commands;
    static map<string,shared_ptr<Gel>> gells;
    static vector<shared_ptr<Gel_to_load>> gel_load_queue;
    static map<string,shared_ptr<Sprite>> sprites;

    static bool clear_flag;

    static SDL_Texture *draw_buffer;

    static mutex com_mutex;

    static void append(shared_ptr<Screen::Command> com);
};

#endif // SCREEN_H
