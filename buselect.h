#ifndef BUSELECT_H
#define BUSELECT_H

#include <string>
#include <vector>
#include <SDL2/SDL.h>

using namespace std;

class BUSelect
{
public:
    BUSelect();

    static void set_title(string &_title);
    static void set_variants(vector<string> &_elements);
    static void show();
    static bool is_selected();
    static int get_result();

    static void draw(SDL_Renderer* r);
    static void events(SDL_Event &e);

private:
    static vector<string> elements;
    static string title;
    static bool selected;
    static bool active;
    static int result;
};

#endif // BUSELECT_H
