<?

/*Компилятор LIS=>BAS для MobileBASIC
**Автор - Андрей Рейгант (HoldFast)
**http://mbteam.ru
**
*/


class LIS
{
    public $data, $file, $save, $coderror, $bufbody, $buffer, $compile = false;
    
    public $lines = array("");
    public $ltypes = array("");
    public $lexems;
    public $vars = array("");
    public $vtypes = array("");
    public $currLine;
    const INT = 0, FLOAT = 1, STR = 2;
    const Operator = 0, Integer = 1, Float = 2, String = 3, Variable = 4, Data = 5, ArrayByte = 6;
    
    public $ops = array("STOP", "POP", "RETURN", "END", "NEW", "RUN", "DIR", "DEG", "RAD", "BYE", "GOTO", "GOSUB", "SLEEP", "PRINT", "REM", "DIM", "IF", "THEN", "CLS", "PLOT", "DRAWLINE", "FILLRECT", "DRAWRECT", "FILLROUNDRECT", "DRAWROUNDRECT", "FILLARC", "DRAWARC", "DRAWSTRING", "SETCOLOR", "BLIT", "FOR", "TO", "STEP", "NEXT", "INPUT", "LIST", "ENTER", "LOAD", "SAVE", "DELETE", "EDIT", "TRAP", "OPEN", "CLOSE", "NOTE", "POINT", "PUT", "GET", "DATA", "RESTORE", "READ", "=", "<>", "<", "<=", ">", ">=", "(", ")", ",", "+", "-", "-", "*", "/", "^", "BITAND", "BITOR", "BITXOR", "NOT", "AND", "OR", "SCREENWIDTH", "SCREENHEIGHT", "ISCOLOR", "NUMCOLORS", "STRINGWIDTH", "STRINGHEIGHT", "LEFT$", "MID$", "RIGHT$", "CHR$", "STR$", "LEN", "ASC", "VAL", "UP", "DOWN", "LEFT", "RIGHT", "FIRE", "GAMEA", "GAMEB", "GAMEC", "GAMED", "DAYS", "MILLISECONDS", "YEAR", "MONTH", "DAY", "HOUR", "MINUTE", "SECOND", "MILLISECOND", "RND", "ERR", "FRE", "MOD", "EDITFORM", "GAUGEFORM", "CHOICEFORM", "DATEFORM", "MESSAGEFORM", "LOG", "EXP", "SQR", "SIN", "COS", "TAN", "ASIN", "ACOS", "ATAN", "ABS", "=", "#", "PRINT", "INPUT", ":", "GELGRAB", "DRAWGEL", "SPRITEGEL", "SPRITEMOVE", "SPRITEHIT", "READDIR$", "PROPERTY$", "GELLOAD", "GELWIDTH", "GELHEIGHT", "PLAYWAV", "PLAYTONE", "INKEY", "SELECT", "ALERT", "SETFONT", "MENUADD", "MENUITEM", "MENUREMOVE", "CALL", "ENDSUB");
    public $keywords = array("STOP", "POP", "RETURN", "END", "RUN", "DIR", "DEG", "RAD", "BYE", "CLS", "ENDSUB");
    public $functions = array("SCREENHEIGHT", "SCREENWIDTH", "ISCOLOR", "NUMCOLORS", "STRINGWIDTH", "STRINGHEIGHT", "LEFT$", "MID$", "RIGHT$", "CHR$", "STR$", "LEN", "ASC", "VAL", "UP", "DOWN", "LEFT", "RIGHT", "FIRE", "GAMEA", "GAMEB", "GAMEC", "GAMED", "DAYS", "MILLISECONDS", "YEAR", "MONTH", "DAY", "HOUR", "MINUTE", "SECOND", "MILLISECOND", "RND", "ERR", "FRE", "MOD", "EDITFORM", "GAUGEFORM", "CHOICEFORM", "DATEFORM", "MESSAGEFORM", "LOG", "EXP", "SQR", "SIN", "COS", "TAN", "ASIN", "ACOS", "ATAN", "ABS", "SPRITEHIT", "READDIR$", "PROPERTY$", "GELWIDTH", "GELHEIGHT", "INKEY", "SELECT", "MENUITEM", "MENUREMOVE");
    
    
    function LIS($name, $newname)
    {
        $this->file = $name;
        $this->save = $newname;
             
    }
    
    
    function init()
    {
        $this->data = file_get_contents($this->file);
    }
    
    function compile()
    {
        if(!$this->compile){
        $this->init();
        $string = explode("\n", $this->data);
        
        for ($i = 0; $i < count($string); $i++) {
            if (trim($string[$i]) != "") {
                $this->lines[] = trim($string[$i]);
                
            }
        }
        
        $this->analize();
        if ($this->coderror != "")
            return $this->coderror;
        $this->compileCode();
        file_put_contents($this->save, $this->buffer);
        $this->compile=true;
       }
    }
    
    
    function analize()
    {
        $this->currLine = 1;
        
        for ($i = 1; $i < count($this->lines); $i++) {
            $this->analizeLine($this->lines[$i]);
            $this->check($this->currLine);
            $this->currLine++;
            if ($this->coderror != "")
                break;
            
        }
        
      
    }
    
    
    function check($lineNum)
    {

    //  for($i=0; $i<count($this->lexems[$lineNum]); $i++){
      //  echo $this->lexems[$lineNum][$i];
      //}
     // echo "\n";

        $braketCount = 0;
        if ($this->ltypes[$lineNum][0] != Integer) {
            $this->coderror = "Invalid line number [" . $this->lexems[$lineNum][0] . "] ";
            // return;
        }
        
        for($i=0; $i<count($this->lexems[$lineNum]); $i++){ 

           if ($this->lexems[$lineNum][$i] == "(")
                $bracketCount++;
            if ($this->lexems[$lineNum][$i] == ")")
                $bracketCount--;
        }

        for($ii=0; $ii<count($this->lexems[$lineNum]); $ii++){ 

               $i = $ii;

            if ($this->ltypes[$lineNum][$i] == Operator) {
                if (in_array($this->lexems[$lineNum][$i], $this->keywords)) {
                    if (!($i == count($this->lexems[$lineNum]) - 1 || $this->lexems[$lineNum][$i + 1] == ":"))
                        $this->coderror = "Operator must have 0 arguments [" . $this->lexems[$lineNum][$i] . "] on line " . $this->lexems[$lineNum][0];
                    return;
                } elseif (in_array($this->lexems[$lineNum][$i], $this->functions)) {
                    if ($i == count($this->lexems[$lineNum]) - 1 || $this->lexems[$lineNum][$i + 1] != "(")
                        $this->coderror = "Error function [" . $this->lexems[$lineNum][$i] . "] on line " . $this->lexems[$lineNum][0];
                    return;
                    
                } elseif (!in_array($this->lexems[$lineNum][$i], $this->functions) && count($this->lexems[$lineNum][$i]) > 1) {
                    if ($i == count($this->lexems[$lineNum]) - 1 || $this->lexems[$lineNum][$i + 1] == ":") {
                        $this->coderror = "Operator must have arguments [" . $this->lexems[$lineNum][$i] . "]";
                        return;
                    } elseif ($this->lexems[$lineNum][$i + 1] == "(" && $this->lexems[$lineNum][$i] != "TO" && $this->lexems[$lineNum][$i] != "STEP") {
                        $this->coderror = "Not an operator [" . $this->lexems[$lineNum][$i] . "]";
                        return;
                    }
                    
                }
                
 
  if ($this->lexems[$lineNum][$i] == "DIM") {
                $this->lexems[$lineNum][$i + 2] = "";
                $this->ltypes[$lineNum][$i + 2] = ArrayByte;
            }
            
            if ($i < count($this->ltypes[$lineNum]) - 1)
                if ($this->ltypes[$lineNum][$i] == Variable && $this->lexems[$lineNum][$i + 1] == "(") { {
                        $this->lexems[$lineNum][$i + 1] = "";
                        $this->ltypes[$lineNum][$i + 1] = ArrayByte;
                    }
                }
            
            }
        }
  
        
        if ($bracketCount != 0) {
            $this->coderror = "Error: any brakets no open/close";
            return;
        }
        
        
    }
    
    
    function analizeLine($line)
    {
        $i = 0;
        while ($i < mb_strlen($line)) {
            $i += $this->readToken($line, $i);
            if ($this->coderror != "")
                break;
            
        }
    }
    
    
    
    function readToken($line, $startPos)
    {
        $initPos = $startPos;
        $op      = "";
        
        if (mb_substr($line, $startPos, 1) >= 'A' && mb_substr($line, $startPos, 1) <= 'Z' || mb_substr($line, $startPos, 1) >= 'a' && mb_substr($line, $startPos, 1) <= 'z') {
            while (mb_substr($line, $startPos, 1) >= 'A' && mb_substr($line, $startPos, 1) <= 'Z' || mb_substr($line, $startPos, 1) >= 'a' && mb_substr($line, $startPos, 1) <= 'z' || mb_substr($line, $startPos, 1) >= '0' && mb_substr($line, $startPos, 1) <= '9' || mb_substr($line, $startPos, 1) == '_' || mb_substr($line, $startPos, 1) == '$' || mb_substr($line, $startPos, 1) == '%') {
                $op .= mb_substr($line, $startPos, 1);
                $startPos++;
                if ($startPos >= mb_strlen($line))
                    break;
            }
            
            
            
            $op = mb_strtoupper($op);
            $this->lexems[$this->currLine][] = $op;
            // echo "лан, не урчи";
            
            $isData = false;
            $data   = "";
            
            if ($op == "DATA" || $op == "REM") {
                $isData = true;
                $startPos++;
                while ($startPos < mb_strlen($line)) {
                    $data .= mb_substr($line, $startPos, 1);
                    $startPos++;
                }
                $this->lexems[$this->currLine][] = $data;
            }
            
            if (!in_array($op, $this->ops)) {
                if (!in_array($op, $this->vars))
                    $this->vars[] = $op;
                if ($this->endWith($op, "$")) {
                    $this->vtypes[] = STR;
                } elseif ($this->endWith($op, "%")) {
                    $this->vtypes[] = INT;
                } else
                    $this->vtypes[] = FLOAT;
                $this->ltypes[$this->currLine][] = Variable;
                // }
                
            } else {
                $this->ltypes[$this->currLine][] = Operator;
                if ($isData) {
                $this->ltypes[$this->currLine][]  = Data;
                }
            }
            
        }
        
        //STRING
        elseif (mb_substr($line, $startPos, 1) == '"') {
            $startPos++;
            while (!(mb_substr($line, $startPos, 1) == '"')) {
                $op .= mb_substr($line, $startPos, 1);
                $startPos++;
                if ($startPos >= mb_strlen($line)) {
                    $this->coderror = "Error complie: Not forund close \"";
                    break;
                }
                
            }
            
            $startPos++;
            $this->lexems[$this->currLine][] = '"' . $op . '"';
            $this->ltypes[$this->currLine][] = String;
            
        }
        // NUMBER
            elseif (mb_substr($line, $startPos, 1) >= '0' && mb_substr($line, $startPos, 1) <= '9') {
            while (mb_substr($line, $startPos, 1) >= '0' && mb_substr($line, $startPos, 1) <= '9' || mb_substr($line, $startPos, 1) == '.' || mb_substr($line, $startPos, 1) == 'e' || mb_substr($line, $startPos, 1) == 'E') {
                $op .= mb_substr($line, $startPos, 1);
                if (mb_substr($line, $startPos, 1) == 'e' || mb_substr($line, $startPos, 1) == 'E') {
                    $op .= mb_substr($line, $startPos + 1, 1);
                    $startPos += 2;
                } else
                    $startPos++;
                if ($startPos >= mb_strlen($line))
                    break;
                
            }
            
            $containPoint = false;
            $error        = false;
            
            for ($i = 0; $i < mb_strlen($op); $i++) {
                if ($op[$i] == '.') {
                    if (!$containPoint)
                        $containPoint = true;
                    else {
                        // exit;
                        $this->coderror = "NumberLexem contains more than one point [" . $op . "]";
                        return;
                    }
                }
            }
            
            if (!$error) {
                $this->lexems[$this->currLine][] = $op;
                if ($containPoint)
                    $this->ltypes[$this->currLine][] = Float;
                else
                    $this->ltypes[$this->currLine][] = Integer;
            }
            
            
        }
        ///SYMBOL
        else {
            if (mb_substr($line, $startPos, 1) != ' ' && mb_substr($line, $startPos, 1) < 'А') {
                $this->lexems[$this->currLine][] = mb_substr($line, $startPos, 1);
                $this->ltypes[$this->currLine][] = Operator;
                $startPos++;
                
            } elseif (mb_substr($line, $startPos, 1) >= 'А' && mb_substr($line, $startPos, 1) <= 'я') {
                $this->coderror = "SymbolLexem not recognized [" . $line[$startPos] . "]";
                return;
                
            } else
                $startPos++;
            
            
        }
        
        return $startPos - $initPos;
    }
    
    
    function compileHead()
    {
        $buf = "";
        $buf .= pack('H*', "4d420001");
        $buf .= pack('n*', count($this->vars) - 1);
        for ($i = 1; $i < count($this->vars); $i++) {
            $buf .= $this->writeUTF($this->vars[$i]);
            switch ($this->vtypes[$i]) {
                case INT:
                    $buf .= pack('H*', "00");
                    break;
                
                case FLOAT:
                    $buf .= pack('H*', "01");
                    break;
                
                case STR:
                    $buf .= pack('H*', "02");
                    break;
            }
            
        }
        $buf .= pack('n*', strlen($this->bufbody));
        $this->buffer = $buf . $this->bufbody;
    }
    
    
    function compileCode()
    {
        $lexems = $this->lexems;
        $ltypes = $this->ltypes;
        
        
        for ($currLine = 1; $currLine < count($lexems) + 1; $currLine++) {
            $line = "";
            $buf .= pack('n*', $lexems[$currLine][0]);
            
            for ($currLex = 1; $currLex < count($lexems[$currLine]); $currLex++) {
                /////Operator
                
                if ($ltypes[$currLine][$currLex] == Operator) {
                    if (trim($lexems[$currLine][$currLex]) == "IF") {
                        $ifStarted = true;
                    } elseif (trim($lexems[$currLine][$currLex]) == "THEN") {
                        $ifStarted = false;
                    } elseif (trim($lexems[$currLine][$currLex]) == "FOR") {
                        $forStarted = true;
                    }
                    
                    for ($i = 0; $i < count($this->ops); $i++) {
                        if (trim($lexems[$currLine][$currLex]) == $this->ops[$i]) {
                            if (trim($lexems[$currLine][$currLex]) == "=") {
                                if ($ifStarted) {
                                    $i = "33";
                                } else if ($forStarted) {
                                    $i          = "7b";
                                    $forStarted = false;
                                } else {
                                    $i = "f6";
                                }
                            } else {
                                $i = dechex($i);
                                if (strlen($i) < 2)
                                    $i = "0" . $i;
                                
                            }
                            ///  echo $i;
                            $line .= pack('H*', $i);
                            break;
                        }
                        
                        
                    }
                    
                    
                    
                }
                /////Variable
                if ($ltypes[$currLine][$currLex] == Variable) {
                    for ($i = 1; $i < count($this->vars); $i++) {
                        if (trim($lexems[$currLine][$currLex]) == trim($this->vars[$i])) {
                            $line .= pack('H*', "FC");
                            $i = dechex($i - 1);
                            //echo $i;
                            if (strlen($i) < 2)
                                $i = "0" . $i;
                            $line .= pack('H*', $i);
                            break;
                        }
                        
                    }
                }
                
                if ($ltypes[$currLine][$currLex] == ArrayByte) {
                    $line .= pack('H*', "F7");
                }
                
                
                if ($ltypes[$currLine][$currLex] == Integer) {
                    if (strlen($lexems[$currLine][$currLex]) > 5)
                        $ltypes[$currLine][$currLex] = Float;
                    else {
                        $val = intval($lexems[$currLine][$currLex]);
                        if ($val <= 127) {
                            $line .= pack('H*', "F8");
                            $i = dechex($val);
                            if (strlen($i) < 2)
                                $i = "0" . $i;
                            $line .= pack('H*', $i);
                            
                        }
                        
                        elseif ($val >= 128 && $val <= 255) {
                            $line .= pack('H*', "F9");
                            $i = dechex($val);
                            if (strlen($i) < 2)
                                $i = "0" . $i;
                            $line .= pack('H*', $i);
                        } elseif ($val >= 256 && $val < 65536) {
                            $line .= pack('H*', "FA");
                            $line .= pack('n*', $val);
                        } else {
                            $line .= pack('H*', "FB");
                            // $line .= pack('I',$val);
                            $line .= pack('c', $this->shiftRight($val, 24) & 0xFF);
                            $line .= pack('c', $this->shiftRight($val, 16) & 0xFF);
                            $line .= pack('c', $this->shiftRight($val, 8) & 0xFF);
                            $line .= pack('c', $this->shiftRight($val, 0) & 0xFF);
                            
                        }
                    }
                }
                /////
                
                if ($ltypes[$currLine][$currLex] == String) {
                    $text = mb_substr($lexems[$currLine][$currLex], 1, mb_strlen($lexems[$currLine][$currLex]) - 2);
                    if (preg_match('//u', $text))
                        $text = iconv("UTF-8", "WINDOWS-1251", $text);
                    $line .= pack('H*', "FD");
                    $line .= $this->writeTEXT($text);
                    
                }
                
                if ($ltypes[$currLine][$currLex] == Data) {
                    $line .= $this->writeTEXT($lexems[$currLine][$currLex]);
                }
                
                if ($ltypes[$currLine][$currLex] == Float) {
                    $line .= pack('H*', "FE");
                    
                    $exp = 0x80;
                    
                    $num = doubleval($lexems[$currLine][$currLex]);
                    if ($num < 1) {
                        while ($num < 1) {
                            $num = $num * 10;
                            $exp--;
                        }
                    } else if ($num >= 10) {
                        while ($num >= 10) {
                            $num = $num / 10;
                            $exp++;
                        }
                    }
                    $radix = ($num * 500000);
                    
                    $line .= pack('c', $this->shiftRight($radix, 16) & 0xFF);
                    $line .= pack('c', $this->shiftRight($radix, 8) & 0xFF);
                    $line .= pack('c', $this->shiftRight($radix, 0) & 0xFF);
                    $line .= pack("c", $exp);
                    
                    
                }
                
                
                
            }
            
            $len = dechex(strlen($line) + 4);
            if (mb_strlen($len) < 2)
                $len = "0" . $len;
            $buf .= pack('H*', $len) . $line . pack('H*', 'FF');
            
        }
        
        $this->bufbody = $buf; 
        $this->compileHead();
        
    }
    
    
    function writeUTF($text)
    {
        $pack = pack('n*', mb_strlen($text));
        
        $pack .= pack('a*', $text);
        
        return $pack;
    }
    
    function writeTEXT($text)
    {
        $len = dechex(strlen($text));
        if (strlen($len) < 2)
            $len = "0" . $len;
        $pack = pack('H*', $len);
        
        $pack .= pack('a*', $text);
        
        return $pack;
    }
    
    
    function shiftRight($a, $b)
    {
        if (is_numeric($a) && $a < 0) {
            return ($a >> $b) + (2 << ~$b);
        } else {
            return ($a >> $b);
        }
    }
    
    
    
    function endWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }
        
        return (substr($haystack, -$length) === $needle);
    }
    
    
}


?>