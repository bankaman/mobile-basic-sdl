
/*
 * libsdl2 wrapper class
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include "sdl_main.h"
#include "g.h"
#include <iostream>

SDL_Main::SDL_Main()
{
    window = nullptr;
    render = nullptr;
    sdl_window_id = 0;
}

SDL_Main::~SDL_Main()
{
    TTF_Quit();
    //delete settings;
    SDL_DestroyRenderer(render);
    SDL_DestroyWindow(window);
}

void SDL_Main::create_window(int x, int y, int width, int height, bool borderless, bool fullscreen)
{
    uint win_settings = SDL_WINDOW_SHOWN|SDL_WINDOW_RESIZABLE;
    if(borderless)
        win_settings = win_settings | SDL_WINDOW_BORDERLESS;
    WIDTH = width;
    HEIGHT = height;
    G::scr_w=WIDTH;
    G::scr_h=HEIGHT;
    window = SDL_CreateWindow("MobileBasic", x, y, WIDTH, HEIGHT,win_settings);
    if(window==nullptr){
        std_debug("Error creating SDL Window: ",SDL_GetError());
    }
    sdl_window_id = SDL_GetWindowID(window);
    is_fullscreen = fullscreen;
    if(fullscreen)
        SDL_SetWindowFullscreen( window, SDL_TRUE );

    if(TTF_Init()==-1)
        std_debug("Error initializing ttf, ",TTF_GetError());
}

void SDL_Main::create_renderer()
{
    if(window==nullptr)
        std::cout<<"No window was created!";

    render = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC|SDL_RENDERER_TARGETTEXTURE);
    if(render == nullptr){
        std_debug("Error creating SDL render:",SDL_GetError());
    }
    SDL_SetRenderDrawColor( render, 0xFF, 0xFF, 0xFF, 0xFF );
    init(render);
}


//------------------------------------ Рендер окна ----------------------------------
void SDL_Main::pre_render(){
    if(render==nullptr)
        return;
    //Clear screen
    SDL_RenderClear( render );

    rendering(render);

    //Update screen
    SDL_RenderPresent( render );
}

void SDL_Main::input_handler(SDL_Event &e)
{
    if(e.window.windowID != sdl_window_id)
        return;

    //Разворачиваем окно на весь экран
    if(e.type == SDL_KEYDOWN){
        if(e.key.keysym.sym == SDLK_RETURN && (e.key.keysym.mod & KMOD_ALT)){
            if(!is_fullscreen){
                SDL_SetWindowDisplayMode(window,&fullscreen_mode);
                SDL_SetWindowFullscreen( window, SDL_TRUE );
                is_fullscreen=true;
            }
        }
        if(e.key.keysym.sym == SDLK_ESCAPE){
            if(is_fullscreen){
                SDL_SetWindowFullscreen( window, SDL_FALSE );
                is_fullscreen = false;
            }
        }
    }
    //События изменения окна
    if( e.type == SDL_WINDOWEVENT ){
        switch(e.window.event){
            case SDL_WINDOWEVENT_SIZE_CHANGED:
                WIDTH = e.window.data1;
                HEIGHT = e.window.data2;
                G::scr_w = WIDTH;
                G::scr_h = HEIGHT;
            break;
        }
    }
    input(e);
}

SDL_Renderer *SDL_Main::get_renderer()
{
    return render;
}

void SDL_Main::set_window_id(int win_id)
{
    window_id = win_id;
}

int SDL_Main::get_width()
{
    return WIDTH;
}

int SDL_Main::get_height()
{
    return HEIGHT;
}

SDL_Window *SDL_Main::get_window()
{
    return window;
}


