#include "basfile.h"

#include <iomanip>

#include "fs.h"
#include "g.h"
#include "console.h"
#include "charset.h"
#include <SDL2/SDL.h>
#include <buselect.h>
#include <bumessagebox.h>
#include <bueditform.h>
#include <screen.h>
#include <keymap.h>
#include <basic_render.h>
#include <files.h>
#include <algorithm>
#include <ctype.h>
#include <ctime>
#include <chrono>
#include "sound.h"

string BasFile::opers[] = {" STOP ", " POP ", " RETURN ", " END ", " NEW ", " RUN ", " DIR ", " DEG ", " RAD ", " BYE ", " GOTO ", " GOSUB ", " SLEEP ", " PRINT ", " REM ", " DIM ", " IF ", " THEN ", " CLS ", " PLOT ", " DRAWLINE ", " FILLRECT ", " DRAWRECT ", " FILLROUNDRECT ", " DRAWROUNDRECT ", " FILLARC ", " DRAWARC ", " DRAWSTRING ", " SETCOLOR ", " BLIT ", " FOR ", " TO ", " STEP ", " NEXT ", " INPUT ", " LIST ", " ENTER ", " LOAD ", " SAVE ", " DELETE ", " EDIT ", " TRAP ", " OPEN ", " CLOSE ", " NOTE ", " POINT ", " PUT ", " GET ", " DATA ", " RESTORE ", " READ ", "=", "<>", "<", "<=", ">", ">=", "(", ")", ",", "+", "-", "-", "*", "/", "^", " BITAND ", " BITOR ", " BITXOR ", " NOT ", " AND ", " OR ", "SCREENWIDTH", "SCREENHEIGHT", " ISCOLOR ", " NUMCOLORS ", "STRINGWIDTH", "STRINGHEIGHT", "LEFT$", "MID$", "RIGHT$", "CHR$", "STR$", "LEN", "ASC", "VAL", " UP ", " DOWN ", " LEFT ", " RIGHT ", " FIRE ", " GAMEA ", " GAMEB ", " GAMEC ", " GAMED ", " DAYS ", " MILLISECONDS ", " YEAR ", " MONTH ", " DAY ", " HOUR ", " MINUTE ", " SECOND ", " MILLISECOND ", "RND", " ERR ", " FRE ", "MOD", "EDITFORM ", "GAUGEFORM ", "CHOICEFORM", "DATEFORM", "MESSAGEFORM", "LOG", "EXP", "SQR", "SIN", "COS", "TAN", "ASIN", "ACOS", "ATAN", "ABS", "=", "#", " PRINT ", " INPUT ", ":", " GELGRAB ", " DRAWGEL ", " SPRITEGEL ", " SPRITEMOVE ", " SPRITEHIT ", "READDIR$", "PROPERTY$", " GELLOAD ", " GELWIDTH", " GELHEIGHT", " PLAYWAV ", " PLAYTONE ", " INKEY", "SELECT", "ALERT ", " SETFONT ", " MENUADD ", " MENUITEM", " MENUREMOVE ", " CALL ", " ENDSUB "};

int BasFile::numeric_deep=0;

BasFile::BasFile()
{

}

BasFile::~BasFile()
{
    clear();
}

bool BasFile::load(string fname)
{
    if(!FS::file_exists(fname)){
        Console::print("File not found: "+fname);
        return false;
    }
    clear();
    bytes = FS::ReadAllBytes(fname);
    cursor = 0;

    //reading file
    int temp = read_as_int4(bytes, cursor);
    if(temp!=0x4d420001){
        std_debug(fname,"not bas file!");
        return false;
    }

    //count of vars
    int var_count=read_as_int2(bytes, cursor);
    std_debug("var_count:",var_count);

    //names of vars
    string varname;
    int type;
    for(int i=0; i<var_count; i++){
        varname = read_as_string(bytes, cursor);
        type = read_as_int1(bytes, cursor);
        std_debug("var: '"+varname+"', type:",type);
        VAR* var;
        var = new VAR();
        var->name=varname;
        var->type=type;
        switch (type) {
            case 0: //int
                var->id = int_vars.size();
                int_vars.push_back(0);
            break;
            case 1: //float
                var->id = float_vars.size();
                float_vars.push_back((float)0.0);
            break;
            case 2: //string
                var->id = string_vars.size();
                string_vars.push_back("");
            break;
        }
        vars.push_back(var);
    }

    //length of code
    int code_len = read_as_int2(bytes, cursor);
    std_debug("code length:",code_len);

    LINE* line;
    while(cursor<bytes.size()){
        int line_num = read_as_int2(bytes, cursor);
        int line_len = read_as_int1(bytes, cursor)-3;
        line = new LINE();
        line->num=line_num;
        line->code.resize(line_len);
        copy(bytes.begin()+cursor,bytes.begin()+cursor+line_len,line->code.begin());
        cursor+=line_len;
        lines.push_back(line);
        //std_debug("line: ",line_num,"len:",line_len);
    }
    std_debug("lines count:",lines.size());
    cur_line=0;
    is_over = false;
    loaded=true;
    current_file = fname;
    current_dir = FS::DirName(fname);
    trap_line=-1;

    return true;
}

void BasFile::run(bool &err)
{
    if(!loaded)
        return;

    int counter=40;
    while(counter>0){
        if(!step(err)){
            break;
        }

        if(err){
            if(trap_line<0)
                break;
            else{
                cur_line=trap_line;
                trap_line=-1;
                err=false;
            }

        }
        counter--;
    }
}

bool BasFile::step(bool &err)
{
    if(cur_line>=lines.size()) is_over=true;
    if(is_over){
        std_debug("Program exited!");
        err=true;
        return false;
    }
    LINE* line;
    VAR*  var=nullptr;
    next_line = cur_line+1;
    line = lines.at(cur_line);
    int cur=0;
    int oper;
    int array_index;

    bool bool_result;
    //std_debug("line: ",dec,line->num, decompile_line(line->code));
    bool done=false;
    bool for_mode=false;
    while(cur<line->code.size() && !done /* !err */){
        oper = 0xff&(int)line->code.at(cur);

        cur++;
        switch (oper) {
            case 0x02:{ // RETURN
                if(pause_stack.size() == 0){
                    std_debug("Error! return without GOSUB at line ", dec, line->num);
                    err = true;
                    break;
                }
                PAUSE_STACK_EL* pauseEl = pause_stack.back();
                cur_line = pauseEl->line;
                next_line = cur_line + 1;
                line = lines.at(pauseEl->line);
                cur = pauseEl->offset;
                delete pauseEl;
                pause_stack.pop_back();
                continue;
            }break;
            case 0x03: // END
                is_over = true;
                //std_debug("END");
                Console::print("Program Ended!");
            break;
            case 0x0C:{ //SLEEP
                int c = numeric_expression(line->code,cur,err);
                //std_debug("SLEEP",c);
                SDL_Delay(c);
            }break;
            case 0x0D:{ //PRINT
                //std_debug("PRINT");
                int c = cur;
                bool e=false;
                string str = string_expression(line->code,c,e);
                if(!e){
                    cur=c;
                    Console::print(str);
                }else{
                    e=false;
                    c=cur;
                    float n = numeric_expression(line->code,c,e);
                    if(!e){
                        cur=c;
                        if(n-(int)n==0)
                            Console::print(to_string((int)n));
                        else
                            Console::print(to_string(n));
                    }else{
                        err=true;
                    }
                }
            }
            break;
            case 0x0E: //REM
                done=true;
            break;
            case 0x0A:{ //GOTO
                int linenum = (int)numeric_expression(line->code,cur,err);
                //std_debug("GOTO",dec,linenum);
                next_line = find_line_id_by_num(linenum);
                if(next_line<0){
                    err=true;
                    std_debug("This line does not exists!", linenum);
                }
            }break;
            case 0x0B:{ //GOSUB
                int linenum = (int)numeric_expression(line->code,cur,err);
                //std_debug("GOSUB",dec,linenum);

                PAUSE_STACK_EL* pauseEl = new PAUSE_STACK_EL();
                pauseEl->line = cur_line;
                pauseEl->offset = cur;
                pause_stack.push_back(pauseEl);

                cur_line = find_line_id_by_num(linenum);
                if(next_line<0){
                    err=true;
                    std_debug("This line does not exists!", linenum);
                }

                return true;
            }break;
            case 0x0f: //DIM
                bas_dim(line->code,cur,err);
            break;
            case 0x10: //IF
                //std_debug("IF");
                bool_result = bool_expression(line->code,cur,err);
                //std_debug(bool_result);
            break;
            case 0x11: //THEN
                //std_debug("THEN");
                if(!bool_result)
                    done=true;
            break;
            case 0x12: //CLS
                Console::clear();
                Screen::bas_cls();
            break;
            case 0x13:
                bas_plot(line->code,cur,err);
            break;
            case 0x15: //FILLRECT
                bas_fillrect(line->code,cur,err);
            break;
            case 0x1e: //FOR
                bas_for(line->code,cur,err);
            break;
            case 0x1b: //DRAWSTRING
                bas_drawstring(line->code,cur,err);
            break;
            case 0x1c: //SETCOLOR
                bas_setcolor(line->code,cur,err);
            break;

            case 0x21: // NEXT
                bas_next(line->code,cur,err);
            break;

            case 0x26: //SAVE
                std_debug("TODO: SAVE PLACEHOLDER");
                string_expression(line->code,cur,err);
            break;

            case 0x27: //DELETE
                bas_delete(line->code,cur,err);
            break;

            case 0x29:{ //TRAP
                int linenum = (int)numeric_expression(line->code,cur,err);
                //std_debug("TRAP",dec,linenum);
                trap_line = find_line_id_by_num(linenum);
                if(trap_line<0){
                    err=true;
                    std_debug("This line does not exists!", linenum);
                }
            }break;

            case 0x2a: //OPEN #
                bas_open(line->code,cur,err);
            break;

            case 0x2b:
                bas_closef(line->code,cur,err);
            break;

            case 0x2e: //PUT #
                bas_putf(line->code,cur,err);
            break;

            case 0x2f: //GET #
                bas_getf(line->code,cur,err);
            break;

            case 0x7d: //PRINT #
                bas_printf(line->code,cur,err);
            break;

            case 0x7e: //INPUT #
                bas_inputf(line->code,cur,err);
            break;

            case 0x7f: // ':'

            break;
            case 0x81: //DRAWGEL
                bas_drawgel(line->code,cur,err);
            break;
            case 0x82:
                bas_spritegel(line->code,cur,err);
            break;
            case 0x83: //SPRITEMOVE
                bas_spritemove(line->code,cur,err);
            break;
            case 0x87: // GELLOAD
                bas_gelload(line->code,cur,err);
            break;
            case 0x8a: // PLAYWAV
                bas_playwav(line->code,cur,err);
            break;
            case 0x8f: //SETFONT
                bas_setfont(line->code,cur,err);
            break;
            case 0x93:{ // CALL
                string filename = string_expression(line->code,cur,err);
                if(filename.at(0)=='/')
                    filename = Charset::utf8_substr(filename,1,Charset::utf8_strlen(filename));
                filename = current_dir+filename;
                std_debug("CALL "+filename);
                Basic_render::getInstance()->bas_call(filename);
                //load(filename);
                /*Screen::set_clear_flag(true);
                while(Screen::get_clear_flag()){
                    SDL_Delay(100);
                }*/
                cur_line = next_line;
                return false;
            }break;
            case 0x94: //ENDSUB
                Basic_render::getInstance()->bas_endsub();
                return false;
            break;
            case 0xfc:{// переменная
                int varnum = read_as_int1(line->code,cur);
                var = vars.at(varnum);
                //std_debug("cur var:",var->name);
            }break;
            case 0xf6: // =
                if(var->type==0){
                    int_vars[var->id] = (int) numeric_expression(line->code,cur,err);
                    //std_debug(var->name,"=",int_vars[var->id]);
                }
                if(var->type==1){
                    float_vars[var->id] = numeric_expression(line->code,cur,err);
                    //std_debug(var->name,"=",float_vars[var->id]);
                }
                if(var->type==2){
                    string_vars[var->id] = string_expression(line->code,cur,err);
                    //std_debug(var->name,"=",string_vars[var->id]);
                }
                if(var->type==3){
                    int_arrays.at(var->id)->at(array_index)=(int)numeric_expression(line->code,cur,err);
                    //std_debug(var->name,"[",array_index,"]=",int_arrays.at(var->id)->at(array_index));
                }
                if(var->type==4){
                    float_arrays.at(var->id)->at(array_index)=numeric_expression(line->code,cur,err);
                    //std_debug(var->name,"[",array_index,"]=",float_arrays.at(var->id)->at(array_index));
                }
                if(var->type==5){
                    string_arrays.at(var->id)->at(array_index)=string_expression(line->code,cur,err);
                    //std_debug(var->name,"[",array_index,"]=",string_arrays.at(var->id)->at(array_index));
                }
                var=nullptr;
            break;
            case 0xf7: //Индекс массива
                array_index = numeric_expression(line->code,cur,err);
                //std_debug("index:",array_index);
            break;
            case 0xff: //Конец строки

            break;
            default:
                std_debug("unk:",hex,oper, try_get_oper(oper));
                err=true;
            break;
        }

    }
    cur_line=next_line;
    if(cur_line>=lines.size()){
        is_over=true;
    }
    return true;
}

void BasFile::reset()
{
    load(current_file);
}

float BasFile::numeric_expression(vector<char> &bytes, int &cursor, bool &err, int priority)
{
    numeric_deep++;
    /*string space;
    for(int i=0;i<numeric_deep;i++) space.append("   |");
    std_debug(space,"numeric >>> #",numeric_deep,"-",priority);*/
    float result=0;
    //std_debug("oper:",hex,oper);
    bool done=false;

    while(!done && !err && (cursor<bytes.size())){
        int oper = read_as_int1(bytes,cursor);
        switch (oper) {
            case 0x39: // скобка (
                //std_debug(space,"(");
                result = numeric_expression(bytes,cursor,err,3);
                //std_debug(space,"(",result,")");
                done = priority==1;
            break;
            case 0x3a: // Скобка )
                if(priority<3)
                    cursor--;
               /* else
                    std_debug(space,")");*/
                done=true;
            break;
            case 0x3c:{ // Оператор '+'

                if((priority==2)||(priority==1)){
                    cursor--;
                    done=true;
                    break;
                }

                //std_debug(space,"+");

                float val=numeric_expression(bytes,cursor,done,1);
                //std_debug(space,result,"+",val);
                result+=val;
                //done=true;
            }break;
            case 0x3e:
            case 0x3d:{ // Оператор '-'


                if((priority==2)||(priority==1)){
                    cursor--;
                    done=true;
                    break;
                }

                //std_debug(space,"-");

                float val = numeric_expression(bytes,cursor,done,1);
                //std_debug(space,result,"-",val);
                result-= val;
                //done=true;
            }break;
            case 0x40:{ // Оператор '/'
                //std_debug(space,"/");
                float val = numeric_expression(bytes,cursor,done,2);
                //std_debug(space,result,"/",val);
                result/=val;
                done=(priority==1);
            }break;
            case 0x3f:{ // Оператор '*'

                //std_debug(space,"*");
                float val = numeric_expression(bytes,cursor,done,2);
                //std_debug(space,result,"*",val);
                result *= val;
                done=priority==1;
            }break;
            case 0x55: //VAL();
                expect_braket(bytes,cursor,err);
                result = stof(string_expression(bytes,cursor,err));
                expect_braket_close(bytes,cursor,err);
            break;
            case 0x5f: // DAYS(0)
                result = bas_days(bytes,cursor,err);
            break;
            case 0x66: //SECOND(days,milsec)
                result = bas_second(bytes,cursor,err);
            break;
            case 0x6c: //EDITFORM
                result = bas_editform(bytes,cursor,err);
            break;
            case 0x8c: //INKEY(0)
                expect_braket(bytes,cursor,err);
                numeric_expression(bytes,cursor,err);
                expect_braket_close(bytes,cursor,err);
                result = Keymap::bas_inkey();
            break;

            case 0xf8: //Целое 1 байт
                result = read_as_int1(bytes,cursor);
                //std_debug(space,result);

            break;
            case 0xf9:
                result = read_as_int1(bytes,cursor);
                //std_debug(space,result);

            break;
            case 0xFA: //Целое 2 байта
                result = read_as_int2(bytes,cursor);
                //std_debug(space,result);

            break;
            case 0xFB: //Целое 4 байта
                result = read_as_int4(bytes,cursor);
            break;
            case 0xFC:{//переменная
                VAR* var = vars.at(read_as_int1(bytes,cursor));

                if(var->type == 0){
                    result = int_vars[var->id];
                    //std_debug(var->name,result);
                }else
                if(var->type == 1){
                    result = float_vars.at(var->id);
                    //std_debug(var->name,result);
                }else
                if(var->type == 2){
                    std_debug("string var in numeric expression");
                    err=true;
                }else if(var->type == 3){
                    int f7 = read_as_int1(bytes,cursor);
                    if(f7!=0xf7){
                        std_debug("array index expected!",var->name);
                        err=true;
                        break;
                    }
                    int index = numeric_expression(bytes,cursor,err);
                    //std_debug(var->name,"[",index,"]");
                    result = int_arrays.at(var->id)->at(index);
                    //std_debug(var->name,"[",index,"]=",result);
                }
                else{
                    err=true;
                    std_debug("unknown var type",var->name,var->type);
                }
                //std_debug(space,result);

            }break;
            case 0xFE://вещественное число
                result = read_as_float(bytes,cursor);
                //std_debug(space,result);

            break;
            case 0x48: // SCREENWIDTH
                //std_debug("SCREENWIDTH");
                expect_braket(bytes,cursor,done);
                numeric_expression(bytes,cursor,done,false);
                //expect_braket_close(bytes,cursor,done);
                result=G::scr_w;
                expect_braket_close(bytes,cursor,err);
            break;
            case 0x49: // SCREENHEIGHT
                //std_debug("SCREENHEIGHT");
                expect_braket(bytes,cursor,done);
                numeric_expression(bytes,cursor,done,false);
                //expect_braket_close(bytes,cursor,done);
                result=G::scr_h;
                //std_debug(result);
                expect_braket_close(bytes,cursor,err);
            break;
            case 0x4c:{ // STRINGWIDTH
                expect_braket(bytes,cursor,err);
                string str = string_expression(bytes,cursor,err);
                result = Basic_render::font.get_text_width(str);
                expect_braket_close(bytes,cursor,err);
            }break;
            case 0x4d: // STRINGHEIGHT
                expect_braket(bytes,cursor,err);
                string_expression(bytes,cursor,err);
                result = Basic_render::font.get_height();
                expect_braket_close(bytes,cursor,err);
            break;
            case 0x53:{ // LEN
                expect_braket(bytes,cursor,err);
                string str =string_expression(bytes,cursor,err);
                result = Charset::utf8_strlen(str);
                expect_braket_close(bytes,cursor,err);
            }
            break;
            case 0x60: //MILLISECONDS
                result = bas_milliseconds(bytes,cursor,err);
            break;
            case 0x68: //RND
                expect_braket(bytes,cursor,err);
                numeric_expression(bytes,cursor,err,false);
                result = rand();
                expect_braket_close(bytes,cursor,err);
            break;
            case 0x6b:{ //MOD
                expect_braket(bytes,cursor,err);
                int in = numeric_expression(bytes,cursor,err,false);
                expect_comma(bytes,cursor,err);
                result = in % (int)numeric_expression(bytes,cursor,err,false);
                expect_braket_close(bytes,cursor,err);
            }
            break;
            case 0x70: // Функция MESSAGEBOX
                result = bas_messagebox(bytes,cursor,err);

            break;
            case 0x7a: // Функция ABS
                expect_braket(bytes,cursor,err);
                result = abs(numeric_expression(bytes,cursor,err));
                expect_braket_close(bytes,cursor,err);
            break;
            case 0x8d: // Функция SELECT
                result = bas_select(bytes,cursor,err);

            break;

            case 0x11: // Оператор THEN
            case 0x1f: // Оператор TO
            case 0x33: // Оператор '=' (bool)
            case 0xf6: // Оператор '=' (num?)
            case 0x34: // <>
            case 0x35: // <
            case 0x36: // <=
            case 0x37: // >
            case 0x38: // >=
            case 0x3b: // Оператор ','
            case 0x46: // Оператор 'AND'
            case 0x47: // Оператор 'OR'
            case 0x7f: // Оператор ':'
                cursor--;
                done=true;
            break;
            case 0xFF:
                done=true;
            break;
            default:
                std_debug("unknown numeric_operand!",hex,oper, try_get_oper(oper));
                err=true;
            break;
        }
    }
    //std_debug(space,"numeric <<< #",numeric_deep);
    numeric_deep--;
    return result;
}

string BasFile::string_expression(vector<char> &bytes, int &cursor, bool &err)
{
    string result;
    bool done=false;
    while((!done) && (cursor<bytes.size())){
        int oper = read_as_int1(bytes,cursor);
        switch (oper) {
            case 0x51:{ //CHR$()
                expect_braket(bytes,cursor,err);
                int b = numeric_expression(bytes,cursor,err);
                expect_braket_close(bytes,cursor,err);
                result.push_back(b);
                //std_debug("CHR$(",b,")=",result);
            }break;
            case 0x52:{ //функция STR$()
                expect_braket(bytes,cursor,err);
                float r = numeric_expression(bytes,cursor,err);
                expect_braket_close(bytes,cursor,err);
                //std_debug("STR$(",r,")");
                if(r-(int)r==0)
                    result = to_string((int)r);
                else
                    result = to_string(r);
            }
            break;
            case 0x85: //Функция READDIR$()
                result = bas_readdir(string_expression(bytes,cursor,err));
                expect_braket_close(bytes,cursor,err);
            break;
            case 0x39: //скобка (
                result = string_expression(bytes,cursor,err);
            break;
            case 0x3A: //скобка )
                cursor--;
                done=true;
            break;
            case 0x3C: //оператор +
                result+= string_expression(bytes,cursor,err);
            break;
            case 0x4e: // Функция LEFT$
                result = bas_left_s(bytes,cursor,err);
            break;
            case 0xFC:{ //Переменная
                int varnum = read_as_int1(bytes,cursor);
                VAR* var = vars.at(varnum);
                if(var->type==2){
                    result = string_vars.at(var->id);
                }
                if(var->type == 0 || var->type == 1){
                    //std_debug("Using numeric var in string expression! ", cur_line);
                    err = true;
                }
            }break;
            case 0xFD: //Строковая константа
                result = read_as_string_1b(bytes,cursor);
            break;

            case 0x11: // Оператор THEN
            case 0x33: // Оператор '='
            case 0x34: // Оператор '<>'
            case 0x3b: // Оператор ','
            case 0x7f: // Оператор ':'
                cursor--;
                done=true;
            break;
            case 0x8c: // INKEY(0)
                done=true;
                err=true;
            break;
            case 0xFF:
                done=true;
            break;
            default:
                std_debug("unk str operand",hex,oper,try_get_oper(oper));
                err=true;
            break;
        }
    }
    return result;
}

bool BasFile::bool_expression(vector<char> &bytes, int &cursor, bool &err)
{
    bool result;
    bool done=false;

    bool is_numeric;
    float numeric_val;
    string strin_val;
    while((!done) && (cursor<bytes.size())){
        int oper = read_as_int1(bytes,cursor);
        switch (oper) {
            case 0x11: // Оператор THEN
                cursor--;
                done=true;
            break;
            case 0x33: //Оператор =
                if(is_numeric){
                    float n = numeric_expression(bytes,cursor,err);
                    result = numeric_val == n;
                    //std_debug(numeric_val,"==",n,"=",result);
                }else{
                    string s = string_expression(bytes,cursor,err);
                    result = strin_val == s;
                    //std_debug(strin_val,"==","'"+s+"'","=",result);
                }
            break;
            case 0x34: //Оператор <>
                if(is_numeric){
                    result = numeric_val != numeric_expression(bytes,cursor,err);
                }else{
                    result = strin_val != string_expression(bytes,cursor,err);
                }

            break;
            case 0x35: //Оператор <
                if(is_numeric){
                    float n = numeric_expression(bytes,cursor,err);
                    result = numeric_val < n;
                    //std_debug(numeric_val,"<",n,"=",result);
                }else{
                    std_debug("Cannot use < on string");
                    err=true;
                }
            break;
            case 0x36: //Оператор <=
                if(is_numeric){
                    float n = numeric_expression(bytes,cursor,err);
                    result = numeric_val <= n;
                }else{
                    std_debug("Cannot use <= on string");
                    err=true;
                }
            break;
            case 0x37: //Оператор >
                if(is_numeric){
                    float n = numeric_expression(bytes,cursor,err);
                    result = numeric_val > n;
                    //std_debug(numeric_val,">",n,"=",result);
                }else{
                    std_debug("Cannot use > on string");
                    err=true;
                }
            break;

            case 0x38: //Оператор >=
                if(is_numeric){
                    float n = numeric_expression(bytes,cursor,err);
                    result = numeric_val >= n;
                }else{
                    std_debug("Cannot use >= on string");
                    err=true;
                }
            break;

            case 0x39: //Оператор (
            case 0x3a: //Оператор )
            break;
            case 0x46:{ // Оператор AND
                bool exp = bool_expression(bytes,cursor,err);
                //std_debug(result,"AND",exp,"=",result && exp);
                bool r = result;
                //std_debug("AND");

                result = (exp && r);


                //err=true;
            }break;
            case 0x47:{ // Оператор OR
                bool exp = bool_expression(bytes,cursor,err);
                result = result || exp;
            }break;
            case 0x51: //CHR$()
                is_numeric = false;
                cursor--;
                strin_val = string_expression(bytes,cursor,err);
            break;
            case 0x56: //Оператор UP(0)
                //std_debug("UP(0)");
                expect_braket(bytes,cursor,err);
                numeric_expression(bytes,cursor,err);
                expect_braket_close(bytes,cursor,err);
                result = Keymap::up;
                is_numeric = true;
                numeric_val = result;
            break;
            case 0x57: //Оператор DOWN(0)
                //std_debug("DOWN(0)");
                expect_braket(bytes,cursor,err);
                numeric_expression(bytes,cursor,err);
                expect_braket_close(bytes,cursor,err);
                result = Keymap::down;
                is_numeric = true;
                numeric_val = result;
            break;
            case 0x58: //Оператор LEFT(0)
                //std_debug("LEFT(0)");
                expect_braket(bytes,cursor,err);
                numeric_expression(bytes,cursor,err);
                expect_braket_close(bytes,cursor,err);
                result = Keymap::left;
                is_numeric = true;
                numeric_val = result;
            break;
            case 0x59: //Оператор RIGHT(0)
                //std_debug("RIGHT(0)");
                expect_braket(bytes,cursor,err);
                numeric_expression(bytes,cursor,err);
                expect_braket_close(bytes,cursor,err);
                result = Keymap::right;
                is_numeric = true;
                numeric_val = result;
            break;
            case 0x5a: //Оператор FIRE(0)
                //std_debug("FIRE(0)");
                expect_braket(bytes,cursor,err);
                numeric_expression(bytes,cursor,err);
                expect_braket_close(bytes,cursor,err);
                result = Keymap::fire;
                is_numeric = true;
                numeric_val = result;
            break;
            case 0x84: //SPRITEHIT
                result = bas_spritehit(bytes,cursor,err);
                numeric_val = result;
                is_numeric = true;
            break;

            case 0x8C: //INKEY(0)
            case 0xf8: //Целое 1 байт
                cursor--;
                numeric_val = numeric_expression(bytes,cursor,err);
                is_numeric = true;
                result = numeric_val!=0;
            break;

            case 0xFC:{ //Переменная
                VAR* var = vars.at(read_as_int1(bytes,cursor));
                if(var->type==0){
                    is_numeric=true;
                    cursor-=2;
                    numeric_val=numeric_expression(bytes,cursor,err);
                }
                if(var->type==1){
                    is_numeric=true;
                    cursor-=2;
                    numeric_val=numeric_expression(bytes,cursor,err);
                }
                if(var->type==2){
                    is_numeric=false;
                    cursor-=2;
                    strin_val=string_expression(bytes,cursor,err);
                }

                if(is_numeric) {
                    result = (numeric_val != 0);
                }else {
                    result = (strin_val.length()!=0);
                }

            }break;

            default:
                std_debug("unk bool operand",hex,oper,try_get_oper(oper));
                err=true;
            break;
        }
    }

    return result;
}

int BasFile::read_as_int4(vector<char> &bytes, int &cursor)
{
    int out=0;
    out = (unsigned char)bytes.at(cursor+3);
    out |= (unsigned char)bytes.at(cursor+2)<<8;
    out |= (unsigned char)bytes.at(cursor+1)<<16;
    out |= (unsigned char)bytes.at(cursor)<<24;
    cursor+=4;
    return out;
}

int BasFile::read_as_int2(vector<char> &bytes, int &cursor)
{
    int out=0;
    out = (unsigned char)bytes.at(cursor+1);
    out |= (unsigned char)bytes.at(cursor)<<8;
    cursor+=2;
    return out;
}

int BasFile::read_as_int2_r(vector<char> &bytes, int &cursor)
{
    int out=0;
    out = (unsigned char)bytes.at(cursor);
    out |= (unsigned char)bytes.at(cursor+1)<<8;
    cursor+=2;
    return out;
}

unsigned char BasFile::read_as_int1(vector<char> &bytes, int &cursor)
{
    cursor++;
    return (unsigned char)bytes.at(cursor-1);
}

string BasFile::read_as_string(vector<char> &bytes, int &cursor)
{
    string out;
    int len = read_as_int2(bytes,cursor);
    for(int i=0;i<len;i++){
        out.push_back(bytes.at(cursor+i));
    }
    cursor+=len;
    return out;
}

string BasFile::read_as_string_1b(vector<char> &bytes, int &cursor)
{
    string out;
    int len = read_as_int1(bytes,cursor);
    for(int i=0;i<len;i++){
        out.push_back(bytes.at(cursor+i));
    }
    cursor+=len;
    out = Charset::to_utf8(out);
    return out;
}

float BasFile::read_as_float(vector<char> &bytes, int &cursor)
{
    float result;
    int base;
    base = (unsigned char)bytes[cursor]<<16;
    base |= (unsigned char)bytes[cursor+1]<<8;
    base |= (unsigned char)bytes[cursor+2];

    double m = base/500000.0;
    int exp = to_s_byte(bytes[cursor+3]);

    //std_debug("base:",base,"exp:",exp);

    double e=1;
    double d;
    if(exp>0){
        d=1;
        for(int i=0;i<exp;i++)
            d*=10;
        e=d;
    }
    if(exp<0){
        d=1;
        for(int i=exp;i<0;i++)
            d/=10;
        e=d;
    }
    //std_debug("m*e","|",m,"*",e);
    result = m*e;

    cursor+=4;
    return result;
}

int BasFile::read_as_varnum(vector<char> &bytes, int &cursor, bool &err)
{
    int check = read_as_int1(bytes,cursor);
    if(check != 0xfc){
        std_debug("Expected a variable!");
        cursor--;
        err=true;
        return -1;
    }
    return read_as_int1(bytes,cursor);
}

int BasFile::read_as_array_index(vector<char> &bytes, int &cursor, bool &err)
{
    int temp = read_as_int1(bytes,cursor);
    if(0xf7!=temp){
        std_debug("f7 byte not found");
        err=true;
        cursor--;
        return -1;
    }
    return numeric_expression(bytes,cursor,err);
}

int BasFile::to_s_byte(int b)
{
    int byte;
    byte = b;

    if (0 <= byte && byte <= 63)
        return byte;
    if (64 <= byte && byte <= 191)
        return (-(128 - byte));
    if (192 <= byte && byte <= 255)
        return (-(256 - byte));
    return 0;

}

string BasFile::decompile_line(vector<char> &bytes)
{
    int cur=0;
    string out="";
    while(cur<bytes.size()){
        int op = read_as_int1(bytes,cur);
        if(op==0xfc){
            VAR* var = vars.at(read_as_int1(bytes,cur));
            out+=var->name;
        }else{
            switch (op) {
                case 0x0e:
                    out.append(" REM \"");
                    out.append(read_as_string_1b(bytes,cur));
                    out.append("\"");
                break;
                case 0xfd:
                    out.append("\"");
                    out.append(read_as_string_1b(bytes,cur));
                    out.append("\"");
                break;
                case 0x30:
                    out.append(" DATA ");
                    out.append(read_as_string_1b(bytes,cur));
                break;
                case 0xf6:
                    out.append("=");
                break;
                case 0xf8:
                case 0xf9:
                    out.append(to_string(read_as_int1(bytes,cur)));
                break;
                case 0xf7:
                    out.append("(");
                break;
                case 0xfa:
                    out.append(to_string(read_as_int2(bytes,cur)));
                break;
                case 0xfb:
                    out.append(to_string(read_as_int4(bytes,cur)));
                break;
                case 0xfe:
                    out.append(to_string(read_as_float(bytes,cur)));
                break;
                case 0xff://конец строки
                    cur++;
                break;
                default:
                    out.append(try_get_oper(op));
                break;
            }
        }
    }
    return out;
}

void BasFile::clear()
{
    for(int i=0;i<vars.size();i++){
        delete vars.at(i);
    }
    for(int i=0;i<pause_stack.size();i++){
        delete pause_stack.at(i);
    }
    pause_stack.clear();
    clear_code();
    /*for(int i=0;i<fors.size();i++)
        delete fors.at(i);*/
    fors.clear();
    vars.clear();
    int_vars.clear();
    float_vars.clear();
    bytes.clear();

    int_arrays.clear();
    float_arrays.clear();
    string_arrays.clear();


}

void BasFile::clear_code()
{
    for(int i=0;i<lines.size();i++)
        delete lines.at(i);
    lines.clear();
    Files::close_all();
    loaded = false;
}

int BasFile::find_line_id_by_num(int num)
{
    int res=-1;
    for(int i=0;i<lines.size();i++)
        if(lines.at(i)->num==num){
            res=i;
            break;
        }
    return res;
}

string BasFile::try_get_oper(int id)
{
    if(id<150)
        return opers[id];
    return string("[noop]");
}

bool BasFile::expect_braket(vector<char> &bytes, int &cursor, bool &err)
{
    int oper = read_as_int1(bytes,cursor);
    if(oper != 0x39){ //(
        std_debug("Expected '(' ");
        err = true;
        return false;
    }
    return true;
}

bool BasFile::expect_braket_close(vector<char> &bytes, int &cursor, bool &err)
{
    int oper = read_as_int1(bytes,cursor);
    if(oper != 0x3a){ //(
        std_debug("Expected '(' ");
        err = true;
        return false;
    }
    return true;
}

bool BasFile::expect_comma(vector<char> &bytes, int &cursor, bool &err)
{
    int oper = read_as_int1(bytes,cursor);
    if(oper != 0x3b){ //(
        std_debug("Expected ',' ");
        err = true;
        return false;
    }
    return true;
}

bool BasFile::expect_equasion(vector<char> &bytes, int &cursor, bool &err)
{
    int oper = read_as_int1(bytes,cursor);
    if(oper != 0x7b){ //(
        std_debug("Expected '=' ");
        err = true;
        return false;
    }
    return true;
}

bool BasFile::expect_sharp(vector<char> &bytes, int &cursor, bool &err)
{
    int oper = read_as_int1(bytes,cursor);
    if(oper != 0x7c){ //(
        std_debug("Expected '#' ");
        err = true;
        return false;
    }
    return true;
}

//READDIR$ TODO: make it normal
string BasFile::bas_readdir(string mask)
{
    string fname = current_dir+mask;
    if(FS::file_exists(fname))
        return mask;
    string out="";
    return out;
}

int BasFile::bas_select(vector<char> &bytes, int &cursor, bool &err)
{
    //std_debug("SELECT");
    if(!expect_braket(bytes,cursor,err))
        return -1;

    //получение заголовка
    string title = string_expression(bytes,cursor,err);
    //std_debug("title:","'"+title+"'", "Elements:");

    //получение элементов
    vector<string> elements;
    int t;
    do{
        if(!expect_comma(bytes,cursor,err)) return -1;
        elements.push_back(string_expression(bytes,cursor,err));
        //std_debug(elements.at(elements.size()-1));
        t=read_as_int1(bytes,cursor);
        cursor--;
    }while(t!=0x3a); // ')'
    cursor++;

    BUSelect::set_title(title);
    BUSelect::set_variants(elements);
    BUSelect::show();

    while (!BUSelect::is_selected()) {
        SDL_Delay(100);
    }

    return BUSelect::get_result();
}

int BasFile::bas_messagebox(vector<char> &bytes, int &cursor, bool &err)
{
    //std_debug("MESSAGEBOX");
    if(!expect_braket(bytes,cursor,err))
        return -1;

    string title = string_expression(bytes,cursor,err);

    if(!expect_comma(bytes,cursor,err))
        return -1;

    string ok = string_expression(bytes,cursor,err);

    if(!expect_comma(bytes,cursor,err))
        return -1;

    string cancel = string_expression(bytes,cursor,err);

    if(!expect_comma(bytes,cursor,err))
        return -1;

    string sub_title = string_expression(bytes,cursor,err);

    if(!expect_comma(bytes,cursor,err))
        return -1;

    string message = string_expression(bytes,cursor,err);

    if(!expect_braket_close(bytes,cursor,err))
        return -1;

    BUMessagebox::show(title,sub_title,message,ok,cancel);

    while (!BUMessagebox::is_selected()) {
        SDL_Delay(100);
    }
}

void BasFile::bas_setcolor(vector<char> &bytes, int &cursor, bool &err)
{
    //std_debug("SETCOLOR");

    int r = (int)numeric_expression(bytes,cursor,err);
    if(!expect_comma(bytes,cursor,err)) return;
    int g = (int)numeric_expression(bytes,cursor,err);
    if(!expect_comma(bytes,cursor,err)) return;
    int b = (int)numeric_expression(bytes,cursor,err);


    Screen::bas_set_color(r,g,b);
}

void BasFile::bas_fillrect(vector<char> &bytes, int &cursor, bool &err)
{
    //std_debug("FILLRECT");
    int x = (int)numeric_expression(bytes,cursor,err);
    if(!expect_comma(bytes,cursor,err)) return;
    int y = (int)numeric_expression(bytes,cursor,err);
    if(!expect_comma(bytes,cursor,err)) return;
    int w = (int)numeric_expression(bytes,cursor,err);
    if(!expect_comma(bytes,cursor,err)) return;
    int h = (int)numeric_expression(bytes,cursor,err);

    Screen::bas_fillrect(x,y,w,h);
}

void BasFile::bas_drawstring(vector<char> &bytes, int &cursor, bool &err)
{
    //std_debug("DRAWSTRING");
    string str = string_expression(bytes,cursor,err);
    if(!expect_comma(bytes,cursor,err)) return;
    int x = (int)numeric_expression(bytes,cursor,err);
    if(!expect_comma(bytes,cursor,err)) return;
    int y = (int)numeric_expression(bytes,cursor,err);

    Screen::bas_drawstring(str,x,y);
}

void BasFile::bas_gelload(vector<char> &bytes, int &cursor, bool &err)
{
    string name = string_expression(bytes,cursor,err);

    if(!expect_comma(bytes,cursor,err)) return;
    string filename = current_dir+string_expression(bytes,cursor,err);

    //std_debug("GELLOAD",name,filename);
    if(!FS::file_exists(filename)){
        std_debug("file not found ","'"+filename+"'");
        err=true;
        return;
    }

    //std_debug("GELLOAD",name,filename);
    Screen::bas_gelload(name,filename);
}

void BasFile::bas_drawgel(vector<char> &bytes, int &cursor, bool &err)
{
    string name = string_expression(bytes,cursor,err);
    if(!expect_comma(bytes,cursor,err)) return;
    int x = (int)numeric_expression(bytes,cursor,err);
    if(!expect_comma(bytes,cursor,err)) return;
    int y = (int)numeric_expression(bytes,cursor,err);

    Screen::bas_drawgel(name,x,y);
}

void BasFile::bas_spritegel(vector<char> &bytes, int &cursor, bool &err)
{
    //std_debug("SPRITEGEL");
    string sprite_name = string_expression(bytes,cursor,err);
    if(!expect_comma(bytes,cursor,err)) return;
    string gel_name = string_expression(bytes,cursor,err);

    Screen::bas_spritegel(sprite_name,gel_name);
}

void BasFile::bas_spritemove(vector<char> &bytes, int &cursor, bool &err)
{
    //std_debug("SPRITEMOVE");
    string sprite_name = string_expression(bytes,cursor,err);
    //std_debug("name:",sprite_name);
    if(!expect_comma(bytes,cursor,err)) return;
    int x = numeric_expression(bytes,cursor,err);
    //std_debug("x:",x);
    if(!expect_comma(bytes,cursor,err)) return;
    int y = numeric_expression(bytes,cursor,err);

    Screen::bas_spritemove(sprite_name,x,y);
}

bool BasFile::bas_spritehit(vector<char> &bytes, int &cursor, bool &err)
{
    if(!expect_braket(bytes,cursor,err)) return false;
    string sprite1 = string_expression(bytes,cursor,err);
    if(!expect_comma(bytes,cursor,err)) return false;
    string sprite2 = string_expression(bytes,cursor,err);
    if(!expect_braket_close(bytes,cursor,err)) return false;

    //std_debug("SPRITEHIT",sprite1,sprite2);
    return Screen::bas_spritehit(sprite1,sprite2);

}

void BasFile::bas_for(vector<char> &bytes, int &cursor, bool &err)
{
    int varnum = read_as_varnum(bytes,cursor,err);
    VAR* var = vars.at(varnum);
    if(var->type==2){
        err=true;
        std_debug("Cannot use string var in FOR!");
        return;
    }
    if(!expect_equasion(bytes,cursor,err)) return;
    int value = numeric_expression(bytes,cursor,err);

    if(var->type==0) int_vars[var->id] = value;
    if(var->type==1) float_vars[var->id] = value;

    int temp = read_as_int1(bytes,cursor);
    if(temp!=0x1f){
        std_debug("Expected 'TO'!");
        err=true;
        return;
    }

    int endval = numeric_expression(bytes,cursor,err);
    int step = 1;
    if(cursor<bytes.size()){
        temp = read_as_int1(bytes,cursor);
        if(temp==0x20){ //STEP
            step = numeric_expression(bytes,cursor,err);
        }else{
            cursor--;
        }
    }


    //std_debug("FOR",var->name,"=",value,"TO",endval,"STEP",step);

    shared_ptr<FOR> for_(new FOR());
    for_->start_line=next_line;
    for_->step=step;
    for_->varnum=varnum;
    for_->endval=endval;

    fors.push_back(for_);
    //err=true;
}

string BasFile::bas_left_s(vector<char> &bytes, int &cursor, bool &err)
{
    string result;
    if(!expect_braket(bytes,cursor,err)) return result;
    string input = string_expression(bytes,cursor,err);
    if(!expect_comma(bytes,cursor,err)) return result;
    int count = numeric_expression(bytes,cursor,err);
    if(!expect_braket_close(bytes,cursor,err)) return result;
    //std_debug("LEFT$(",input,count);
    result = Charset::utf8_substr(input,0,count);
    return result;
}

void BasFile::bas_next(vector<char> &bytes, int &cursor, bool &err)
{
    int varnum = read_as_varnum(bytes,cursor,err);

    VAR* var = vars.at(varnum);

    shared_ptr<FOR> f = nullptr;
    int for_id;
    for(int i=0;i<fors.size();i++){
        if(fors.at(i)->varnum==varnum){
            f = fors.at(i);
            for_id=i;
            break;
        }
    }
    if(f==nullptr){
        std_debug("For cycle not exists for var",var->name);
        return;
    }
    bool over=false;
    if(var->type==0){
        int_vars[var->id]+=f->step;
        if(int_vars[var->id]>f->endval) over=true;
        //std_debug("NEXT",var->name,"=",int_vars[var->id],"endval:",f->endval);
    }
    if(var->type==1){
        float_vars[var->id]+=f->step;
        if(float_vars[var->id]>f->endval) over=true;
        //std_debug("NEXT",var->name,"=",float_vars[var->id],"endval:",f->endval);
    }
    //err=true;

    if(over){
        fors.erase(fors.begin()+for_id);
        return;
    }

    next_line=f->start_line;


}

void BasFile::bas_setfont(vector<char> &bytes, int &cursor, bool &err)
{
    int size = numeric_expression(bytes,cursor,err);

    Basic_render::font.set_size(14+size);
}

void BasFile::bas_open(vector<char> &bytes, int &cursor, bool &err)
{
    if(!expect_sharp(bytes,cursor,err)) return;
    int channel = numeric_expression(bytes,cursor,err);
    if(!expect_comma(bytes,cursor,err)) return;
    string fname = current_dir+string_expression(bytes,cursor,err);
    if(!expect_comma(bytes,cursor,err)) return;
    string mode = string_expression(bytes,cursor,err);

    transform(mode.begin(), mode.end(), mode.begin(), (int (*)(int))tolower);

    if(mode!="input" && mode!="output"){
        err=true;
        std_debug("unknown file mode",mode);
        return;
    }

    //std_debug("OPEN ",channel,fname,mode);
    Files::open_channel(channel,fname,mode,err);
}

void BasFile::bas_inputf(vector<char> &bytes, int &cursor, bool &err)
{
    if(!expect_sharp(bytes,cursor,err)) return;
    int channel = numeric_expression(bytes,cursor,err);
    if(!expect_comma(bytes,cursor,err)) return;
    int varnum = read_as_varnum(bytes,cursor,err);
    VAR* var = vars.at(varnum);

    if(var->type==0){
        int_vars[var->id]= Files::input_int(channel,err);
        //std_debug(var->name,"=",int_vars[var->id]);
    }
    if(var->type==1){
        //err=true;
        float_vars[var->id]= Files::input_float(channel,err);
        //std_debug(var->name,"=",float_vars[var->id]);
    }
    if(var->type==2){
        string_vars[var->id] = Files::input_string(channel,err);
    }
}

void BasFile::bas_dim(vector<char> &bytes, int &cursor, bool &err)
{
    int varnum = read_as_varnum(bytes,cursor,err);
    VAR* var=vars.at(varnum);

    int temp = read_as_int1(bytes,cursor);
    if(0xf7!=temp){
        std_debug("f7 byte not found");
        err=true;
        return;
    }

    int count = numeric_expression(bytes,cursor,err);
    //std_debug("DIM",var->name,"(",count,")");

    var->type+=3;
    if(var->type==3){
        var->id = int_arrays.size();
        shared_ptr<vector<int>> ar(new vector<int>(count));
        //std_debug(count);
        //err=true;
        for(int i=0;i<count;i++)
            ar->push_back(0);
        int_arrays.push_back(ar);
    }
    if(var->type==4){
        std_debug("Todo: float arrays");
        err=true;
    }
    if(var->type==5){
        std_debug("Todo: string arrays");
        err=true;
    }
}

void BasFile::bas_putf(vector<char> &bytes, int &cursor, bool &err)
{
    if(!expect_sharp(bytes,cursor,err)) return;
    int channel = numeric_expression(bytes,cursor,err);
    if(!expect_comma(bytes,cursor,err)) return;

    int byte = numeric_expression(bytes, cursor, err);
    Files::put_byte(channel, byte, err);
}

void BasFile::bas_getf(vector<char> &bytes, int &cursor, bool &err)
{
    if(!expect_sharp(bytes,cursor,err)) return;
    int channel = numeric_expression(bytes,cursor,err);
    if(!expect_comma(bytes,cursor,err)) return;

    int byte = Files::get_byte(channel,err);
    int varnum = read_as_varnum(bytes,cursor,err);
    VAR* var = vars.at(varnum);

    if(var->type!=0 && var->type!=3){
        //std_debug("var",var->name,"is not integer!");
        err=true;
        return;
    }

    if(var->type==0){
        int_vars[var->id] = byte;
        //std_debug(var->name,"=",int_vars[var->id]);
    }
    if(var->type==3){
        int index = read_as_array_index(bytes,cursor,err);
        //std_debug(var->name,"[",index,"]",int_arrays.at(var->id)->at(index));
        int_arrays.at(var->id)->at(index) = byte;
        //std_debug(var->name,"[",index,"]=",int_arrays.at(var->id)->at(index));
    }

}

void BasFile::bas_closef(vector<char> &bytes, int &cursor, bool &err)
{
    if(!expect_sharp(bytes,cursor,err)) return;
    int channel = numeric_expression(bytes,cursor,err);

    Files::close_channel(channel,err);
}

void BasFile::bas_printf(vector<char> &bytes, int &cursor, bool &err)
{
    if(!expect_sharp(bytes,cursor,err)) return;
    int channel = numeric_expression(bytes,cursor,err);
    if(!expect_comma(bytes,cursor,err)) return;

    int varnum = read_as_varnum(bytes,cursor,err);

    if(varnum<0){
        err = false;
        Files::print_string(channel, string_expression(bytes, cursor, err), err);
        return;
    }

    VAR* var = vars.at(varnum);

    if(var->type==0){
        Files::print_int(channel,int_vars[var->id],err);
    }
    if(var->type==1){
        //err=true;
        Files::print_float(channel,float_vars[var->id],err);
    }
    if(var->type==2){
        Files::print_string(channel,string_vars[var->id],err);
    }
}

void BasFile::bas_plot(vector<char> &bytes, int &cursor, bool &err)
{
    int x = numeric_expression(bytes,cursor,err);
    if(!expect_comma(bytes,cursor,err)) return;
    int y = numeric_expression(bytes,cursor,err);
    Screen::bas_plot(x,y);
}

int BasFile::bas_days(vector<char> &bytes, int &cursor, bool &err)
{
    if(!expect_braket(bytes,cursor,err)) return 0;
    numeric_expression(bytes,cursor,err);
    if(!expect_braket_close(bytes,cursor,err)) return 0;

    time_t t = time(nullptr);
    int ret = t/60/60/24;

    return ret;
}

int BasFile::bas_milliseconds(vector<char> &bytes, int &cursor, bool &err)
{
    if(!expect_braket(bytes,cursor,err)) return 0;
    numeric_expression(bytes,cursor,err);
    if(!expect_braket_close(bytes,cursor,err)) return 0;

    auto now = std::chrono::system_clock::now();

    auto duration = now.time_since_epoch();
    auto millis = std::chrono::duration_cast<chrono::milliseconds>(duration).count();


    return millis%(1000*60*60*24);

}

int BasFile::bas_second(vector<char> &bytes, int &cursor, bool &err)
{
    if(!expect_braket(bytes,cursor,err)) return 0;
    numeric_expression(bytes,cursor,err);
    if(!expect_comma(bytes,cursor,err)) return 0;
    int ms = numeric_expression(bytes,cursor,err);
    if(!expect_braket_close(bytes,cursor,err)) return 0;

    return ms/1000%60;
}

void BasFile::bas_playwav(vector<char> &bytes, int &cursor, bool &err)
{
    string fname = current_dir+string_expression(bytes,cursor,err);
    string type = fname.substr(fname.length()-4);
    transform(type.begin(), type.end(), type.begin(), (int (*)(int))tolower);

    if(!FS::file_exists(fname)){
        std_debug("File not found!",fname);
        err=true;
        return;
    }

    if(type == ".mid"){
        Sound::play_midi(fname);
    }else{
        err=true;
        std_debug("unknown file type ",fname);
    }

}

void BasFile::bas_delete(vector<char> &bytes, int &cursor, bool &err)
{
    string fname = current_dir+string_expression(bytes,cursor,err);
    unlink(fname.c_str());
}

int BasFile::bas_editform(vector<char> &bytes, int &cursor, bool &err)
{
    //std_debug("EDITFORM");
    if(!expect_braket(bytes,cursor,err))
        return -1;

    string title = string_expression(bytes,cursor,err);

    if(!expect_comma(bytes,cursor,err))
        return -1;

    string ok = string_expression(bytes,cursor,err);

    if(!expect_comma(bytes,cursor,err))
        return -1;

    string cancel = string_expression(bytes,cursor,err);

    if(!expect_comma(bytes,cursor,err))
        return -1;

    string sub_title = string_expression(bytes,cursor,err);

    if(!expect_comma(bytes,cursor,err))
        return -1;

    int varnum = read_as_varnum(bytes,cursor,err);

    if(!expect_comma(bytes,cursor,err))
        return -1;

    int max_length = numeric_expression(bytes,cursor,err);

    if(!expect_comma(bytes,cursor,err))
        return -1;

    int edit_type = numeric_expression(bytes,cursor,err);

    if(!expect_braket_close(bytes,cursor,err))
        return -1;

    string var_cont;

    VAR* var=vars.at(varnum);
    if(var->type==0){
        var_cont = to_string(int_vars.at(var->id));
    }else if(var->type==1){
        var_cont = to_string(float_vars.at(var->id));
    }else if(var->type==2){
        var_cont = string_vars.at(var->id);
    }else{
        std_debug("unknown var type:",var->name,var->type);
        err=true;
        return -1;
    }



    BUEditform::show(title,sub_title,var_cont,ok,cancel);

    while (!BUEditform::is_selected()) {
        SDL_Delay(100);
    }
    int result = BUEditform::get_result();

    if(result>0){
        if(var->type==0){
            int_vars[var->id] = stoi(BUEditform::get_text());
        }else if(var->type==1){
            float_vars[var->id] = stof(BUEditform::get_text());
        }else if(var->type==2){
            string_vars[var->id] = BUEditform::get_text();
        }else{
            std_debug("unknown var type:",var->name,var->type);
            err=true;
            return -1;
        }
    }
    return result;
}

