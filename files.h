#ifndef FILES_H
#define FILES_H

#include <string>
#include <fstream>
#include <map>
#include <memory>

using namespace std;

class Files
{

public:
    Files();

    static void open_channel(int channel, string &file_name, string &mode ,bool &err);
    static void close_channel(int channel, bool &err);
    static int input_int(int channel,bool &err);
    static float input_float(int channel,bool &err);
    static string input_string(int channel,bool &err);
    static void print_int(int channel,int val,bool &err);
    static void print_float(int channel,float val,bool &err);
    static void print_string(int channel, string str, bool &err);
    static int get_byte(int channel,bool &err);
    static void put_byte(int channel, int byte, bool &err);

    static void close_all();

private:
    static map<int,shared_ptr<fstream>> channels;
};

#endif // FILES_H
