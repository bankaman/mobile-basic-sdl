#ifndef BASIC_RENDER_H
#define BASIC_RENDER_H

#include <sdl_main.h>
#include <font.h>
#include <string>
#include <basfile.h>

using namespace std;


class Basic_render : public SDL_Main
{
public:
    Basic_render();
    ~Basic_render();

    static Font font;

    static Basic_render* instance;
    static Basic_render* getInstance();

    bool load_file(string fname);
    static Uint32 bas_process(Uint32 interval, void *param);
    void bas_call(string &filename);
    void bas_endsub();
    BasFile* getCurrentBas();
    static bool console_mode;
    static bool step_mode;
    static bool step_done;

private:
    void init(SDL_Renderer *r);
    void rendering(SDL_Renderer *r);
    void input(SDL_Event &e);

    SDL_TimerID bas_process_timer;
    BasFile* bas;
    vector<BasFile*> basStack;

};

#endif // BASIC_RENDER_H
