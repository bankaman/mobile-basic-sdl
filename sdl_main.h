#ifndef SDL_MAIN_H
#define SDL_MAIN_H

/*
 * libsdl wrapper class
 * Copyright Tomilin Dmitriy <bankastudio@gmail.com>, 2018
*/

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "glob.h"

class SDL_Main
{
public:
    SDL_Main();
    virtual ~SDL_Main();
    void pre_render();
    void create_window(int x, int y, int width, int height, bool borderless, bool fullscreen);
    void create_renderer();
    void input_handler(SDL_Event &e);
    SDL_Renderer *get_renderer();
    void set_window_id(int win_id);
    int get_width();
    int get_height();
    SDL_Window* get_window();



protected:
    SDL_Renderer* render;
    SDL_Window* window;
    int window_id;

private:
    int WIDTH=1024;
    int HEIGHT=576;

    SDL_DisplayMode fullscreen_mode = {SDL_PIXELFORMAT_RGBA8888,1920,1080,60,nullptr};

    //int current_w = WIDTH, current_h = HEIGHT;
    bool is_fullscreen=false;

    virtual void init(SDL_Renderer *r){}
    virtual void rendering(SDL_Renderer* r){}
    virtual void input(SDL_Event &e){}


    uint sdl_window_id;

};

#endif // SDL_MAIN_H
