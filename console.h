#ifndef CONSOLE_H
#define CONSOLE_H

#include <string>
#include <vector>

using namespace std;

class Console
{
public:
    Console();

    static void draw();
    static void print(string str);
    static void clear();

private:

    static std::vector<std::string> explode(std::string const & s, char delim);
    static vector<string> console;

};

#endif // CONSOLE_H
