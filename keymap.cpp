#include "keymap.h"
#include "g.h"

bool Keymap::fire=false;
bool Keymap::up=false;
bool Keymap::right=false;
bool Keymap::left=false;
bool Keymap::down=false;
int  Keymap::last_inkey_id=-1;
vector<int> Keymap::keys;

Keymap::Keymap()
{

}

void Keymap::register_key(int code)
{
    //std_debug("register code:",code);
    if(get_id_by_code(code)<0)
        keys.push_back(code);
}

void Keymap::unregister_key(int code)
{

    int id = get_id_by_code(code);
    //std_debug("unreg code:",code,"| id:",id);
    if(id>=0)
        keys.erase(keys.begin()+id);
}

int Keymap::bas_inkey()
{
    if(keys.size()==0)
        return 0;
    last_inkey_id++;
    if(last_inkey_id>=keys.size())
        last_inkey_id=0;
    return keys.at(last_inkey_id);
}

int Keymap::get_id_by_code(int code)
{
    int ret=-1;
    for(int i=0;i<keys.size();i++){
        if(keys[i]==code){
            ret=i;
            break;
        }
    }
    return ret;
}
