#ifndef CHARSET_H
#define CHARSET_H

#include <string>

using namespace std;

class Charset
{
public:
    Charset();
    static string to_utf8(string str);
    static int utf8_strlen(string &str);
    static string utf8_substr(const string& str, unsigned int start, unsigned int leng);
};

#endif // CHARSET_H
