#include "buselect.h"
#include "console.h"
#include "g.h"
#include "basic_render.h"

vector<string> BUSelect::elements;
string BUSelect::title;
bool BUSelect::selected;
bool BUSelect::active=false;
int BUSelect::result;

BUSelect::BUSelect()
{

}

void BUSelect::set_title(string &_title)
{
    title = _title;
}

void BUSelect::set_variants(vector<string> &_elements)
{
    elements = _elements;
}

void BUSelect::show()
{
    selected=false;
    active=true;
    result=0;
}

bool BUSelect::is_selected()
{
    return selected;
}

int BUSelect::get_result()
{
    return result;
}

void BUSelect::draw(SDL_Renderer* r)
{
    if(!active)
        return;
    SDL_Rect rect;
    rect.x=10;
    rect.y=10;
    rect.w=50;
    rect.h=50;
    Uint8 red,green,blue,alpha;

    Font* font = &Basic_render::font;

    rect.w = font->get_text_width(title);
    for(int i=0;i<elements.size();i++){
        if(rect.w<font->get_text_width(elements.at(i)))
            rect.w = font->get_text_width(elements.at(i));
    }
    rect.w+=20;
    rect.h = (elements.size()+1)*Basic_render::font.get_height()+50;

    rect.x = G::scr_w/2-rect.w/2;
    rect.y = G::scr_h/2-rect.h/2;

    SDL_RenderFillRect(r,&rect);
    SDL_GetRenderDrawColor(r,&red,&green,&blue,&alpha);
    SDL_SetRenderDrawColor(r,0,0,0,0xff);

    font->drawText( title,
                    rect.x+rect.w/2-font->get_text_width(title)/2,
                    rect.y+10
                );
    for(int i=0;i<elements.size();i++){
        string e;
        e = elements.at(i);
        if(i==result){
            SDL_Rect sel;
            sel.x=rect.x;
            sel.w=rect.w;
            sel.y=i*font->get_height()+rect.y+rect.h-elements.size()*font->get_height()-10;
            sel.h=font->get_height();
            SDL_RenderFillRect(r,&sel);
            font->set_color({0xff,0xff,0xff});
        }else{
            SDL_SetRenderDrawColor(r,0x00,0x00,0x00,0xff);
        }
        font->drawText( e,
                        rect.x+rect.w/2-font->get_text_width(e)/2,
                        i*font->get_height()+rect.y+rect.h-elements.size()*font->get_height()-10
                    );
        if(i==result)
            font->return_color();

    }


    SDL_RenderDrawRect(r,&rect);
    SDL_SetRenderDrawColor(r,red,green,blue,alpha);


}

void BUSelect::events(SDL_Event &e)
{
    if(!active)
        return;

    if(e.type == SDL_KEYDOWN){
        if(e.key.keysym.sym==SDLK_DOWN){
            result++;
            if(result>=elements.size())
                result=0;
        }
        if(e.key.keysym.sym==SDLK_UP){
            result--;
            if(result<0)
                result=elements.size()-1;
        }

        if(e.key.keysym.sym==SDLK_RETURN){
            selected=true;
            active=false;
        }
    }
}
