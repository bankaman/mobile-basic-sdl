#include <iostream>
#include <stdio.h>

#include <SDL2/SDL.h>
#include <sdl_main.h>

#include <g.h>
#include <fs.h>
#include <basic_render.h>

int main(int argc, const char** argv)
{
    string binary = argv[0];
    G::binary_dir = FS::DirName(binary);
    std_debug("binary dir:",G::binary_dir);


    Basic_render win;
    win.create_window(600,0,800,600,false,false);
    win.create_renderer();


    if(argc>1){
        win.load_file(argv[1]);
    }


    bool do_main_loop=true;
    SDL_Event e;
    while(do_main_loop){
        //Events handler

        while( SDL_PollEvent( &e ) != 0 ) {
            /*if(e.type==SDL_KEYDOWN)
                if(e.key.keysym.sym==27)
                    do_main_loop=false;*/
            if(e.type == SDL_QUIT) do_main_loop=false;
            /*for(int i=0;i<windows.length();i++){
                windows[i]->input_handler(e);
            }*/
            win.input_handler(e);

        }

        win.pre_render();

        SDL_Delay(16);
    }

    return 0;
}
