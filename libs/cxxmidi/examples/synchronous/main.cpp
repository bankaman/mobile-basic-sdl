#include <cxxmidi/file.hpp>
#include <cxxmidi/output/default.hpp>
#include <cxxmidi/player/synchronous.hpp>
#include <cxxmidi/player/asynchronous.hpp>
#include <cxxmidi/callback.hpp>

int main(int /*argc*/, char ** /*argv*/)
{
    CxxMidi::Output::Default output(1);
    //CxxMidi::Player::Synchronous player(&output);
    CxxMidi::Player::Asynchronous player(&output);


    CxxMidi::File file("/home/banka/QT/MobileBasicSDL/libs/cxxmidi/music/chopin.mid");
    player.setFile(&file);


    player.play();

    CxxMidi::Sleep::us(1000 * 1000 * 20); // 5 sec
    player.pause();
}
