#include "sound.h"
#include "g.h"


unique_ptr<CxxMidi::Output::Default> Sound::midi_output;
unique_ptr<CxxMidi::Player::Asynchronous> Sound::midi_player;
unique_ptr<CxxMidi::File> Sound::midi_file;

Sound::Sound()
{

}

void Sound::init(int channel)
{
    return;
    CxxMidi::Output::Default *output = new CxxMidi::Output::Default(channel);
    CxxMidi::Player::Asynchronous *player = new CxxMidi::Player::Asynchronous(output);

    midi_output = unique_ptr<CxxMidi::Output::Default>(output);
    midi_player = unique_ptr<CxxMidi::Player::Asynchronous>(player);

    player->setCallbackFinished(Sound::midi_callback_finished);
}

void Sound::play_midi(string filename)
{
    return;
    CxxMidi::File *file = new CxxMidi::File(filename.c_str());
    midi_file = unique_ptr<CxxMidi::File>(file);
    if(midi_player->isPlaying())
        midi_player->pause();

    //if(midi_player->finished()){
    midi_player->setFile(file);
    /*CxxMidi::Time::Point tp;
    midi_player->goTo(tp.zero());*/
    //}

    midi_player->play();
}

void Sound::midi_callback_finished()
{

    std_debug("midi finished!");
}
