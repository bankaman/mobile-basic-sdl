#ifndef SOUND_H
#define SOUND_H

#include <cxxmidi/file.hpp>
#include <cxxmidi/output/default.hpp>
#include <cxxmidi/player/asynchronous.hpp>
#include <cxxmidi/callback.hpp>

#include <string>
#include <memory>

using namespace std;

class Sound
{
public:
    Sound();

    static void init(int channel);

    static void play_midi(string filename);

private:
    static void midi_callback_finished();

    static unique_ptr<CxxMidi::Output::Default> midi_output;
    static unique_ptr<CxxMidi::Player::Asynchronous> midi_player;
    static unique_ptr<CxxMidi::File> midi_file;
};

#endif // SOUND_H
