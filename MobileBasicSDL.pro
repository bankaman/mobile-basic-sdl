TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lSDL2 -lSDL2_ttf -lSDL2_image

unix:!macx {
    LIBS += -lpthread
    LIBS += /usr/lib/x86_64-linux-gnu/libasound.so
    #LIBS += /usr/lib/i386-linux-gnu/libasound.so
}
win32 {
    LIBS += winmm.lib
}

TARGET = MobileBasicSDL

INCLUDEPATH += "./libs/cxxmidi/include"

SOURCES += main.cpp \
    sdl_main.cpp \
    basic_render.cpp \
    font.cpp \
    g.cpp \
    basfile.cpp \
    fs.cpp \
    console.cpp \
    charset.cpp \
    buselect.cpp \
    bumessagebox.cpp \
    screen.cpp \
    texture.cpp \
    keymap.cpp \
    files.cpp \
    sound.cpp \
    bueditform.cpp

HEADERS += \
    sdl_main.h \
    basic_render.h \
    font.h \
    g.h \
    basfile.h \
    fs.h \
    console.h \
    charset.h \
    buselect.h \
    bumessagebox.h \
    screen.h \
    texture.h \
    keymap.h \
    files.h \
    sound.h \
    bueditform.h




