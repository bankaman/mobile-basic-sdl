#include "bumessagebox.h"
#include "SDL2/SDL.h"
#include "font.h"
#include "basic_render.h"
#include "g.h"


string BUMessagebox::title;
string BUMessagebox::sub_title;
string BUMessagebox::message;
string BUMessagebox::ok_btn;
string BUMessagebox::cancel_btn;
bool BUMessagebox::selected;
bool BUMessagebox::active=false;
int BUMessagebox::result;

BUMessagebox::BUMessagebox()
{

}

void BUMessagebox::show(string &_title, string &_sub_title, string &_message, string &_ok_btn, string &_cancel_btn)
{
    title = _title;
    sub_title = _sub_title;
    message = _message;
    ok_btn = _ok_btn;
    cancel_btn = _cancel_btn;

    active = true;
    selected = false;
    result = 1;


}

bool BUMessagebox::is_selected()
{
    return selected;
}

int BUMessagebox::get_result()
{
    return result;
}

void BUMessagebox::draw(SDL_Renderer *r)
{
    if(!active)
        return;

    SDL_Rect rect;
    rect.x=10;
    rect.y=10;
    rect.w=50;
    rect.h=50;
    Uint8 red,green,blue,alpha;

    //SDL_SetTextInputRect(&rect);

    Font* font = &Basic_render::font;

    rect.w = font->get_text_width(message)+10;
    rect.h = font->get_height()*6;
    rect.x = G::scr_w/2 - rect.w/2;
    rect.y = G::scr_h/2 - rect.h/2;

    SDL_RenderFillRect(r,&rect);
    SDL_GetRenderDrawColor(r,&red,&green,&blue,&alpha);
    SDL_SetRenderDrawColor(r,0,0,0,0xff);
    SDL_RenderDrawRect(r,&rect);

    font->drawText(title,rect.x+rect.w/2-font->get_text_width(title)/2,rect.y+5);
    font->drawText(sub_title,rect.x+rect.w/2-font->get_text_width(sub_title)/2,rect.y+5+font->get_height());
    font->drawText(message,rect.x+rect.w/2-font->get_text_width(message)/2,rect.y+10+font->get_height()*2);
    SDL_Rect sel;
    if(result==-1){
        font->set_color({0,0,0});
        sel.x=rect.x;
        sel.w=rect.w/2;
        sel.y=rect.y+rect.h-font->get_height()-5;
        sel.h=font->get_height()+5;
        SDL_RenderFillRect(r,&sel);
        font->set_color({0xff,0xff,0xff});
    }

    font->drawText(cancel_btn,
                       rect.x+rect.w/4-font->get_text_width(cancel_btn)/2,
                       rect.y+rect.h-font->get_height()-5
                   );

    if(result==-1){
        font->return_color();
    }

    if(result==1){
        font->set_color({0,0,0});
        sel.x=rect.x+rect.w/2;
        sel.w=rect.w/2;
        sel.y=rect.y+rect.h-font->get_height()-5;
        sel.h=font->get_height()+5;
        SDL_RenderFillRect(r,&sel);
        font->set_color({0xff,0xff,0xff});
    }

    font->drawText(ok_btn,
                       rect.x+rect.w/2+rect.w/4-font->get_text_width(ok_btn)/2,
                       rect.y+rect.h-font->get_height()-5
                   );

    if(result==1){
        font->return_color();
    }


    SDL_SetRenderDrawColor(r,red,green,blue,alpha);
}

void BUMessagebox::events(SDL_Event &e)
{
    if(!active)
        return;
    if(e.type == SDL_KEYDOWN){
        if(e.key.keysym.sym==SDLK_LEFT){
            result=-1;
        }
        if(e.key.keysym.sym==SDLK_RIGHT){
            result=1;
        }
        if(e.key.keysym.sym==SDLK_RETURN){
            selected=true;
            active=false;
        }
    }


}
