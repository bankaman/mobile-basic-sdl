#include "basic_render.h"

#include <iostream>
#include <g.h>
#include <console.h>
#include <buselect.h>
#include <bumessagebox.h>
#include <screen.h>
#include <keymap.h>
#include <files.h>
#include <sound.h>
#include <bueditform.h>

Font Basic_render::font;
Basic_render* Basic_render::instance;
bool Basic_render::console_mode;
bool Basic_render::step_mode;
bool Basic_render::step_done;

Basic_render::Basic_render()
{
    console_mode=true;
    step_mode = true;
    step_done =true;
    instance = this;
}

Basic_render::~Basic_render()
{
    SDL_RemoveTimer(bas_process_timer);
    Screen::clear();
    Screen::free();
    Files::close_all();

    delete bas;
    for(int i=0; i<basStack.size(); i++){
        delete basStack.at(i);
    }
    basStack.clear();
}

Basic_render *Basic_render::getInstance()
{
    return instance;
}

bool Basic_render::load_file(string fname)
{
    bas = new BasFile();
    if(bas->load(fname)){
        Console::print("file '"+fname+"' was loaded.");
        return true;
    }
    return false;
}

Uint32 Basic_render::bas_process(Uint32 interval, void *param)
{
    BasFile* _bas = getInstance()->getCurrentBas();
    if(!step_mode){
        bool err=false;
        _bas->run(err);
        if(err)
            step_mode=true;
    }else{
        bool err=false;
        if(!step_done){
            _bas->step(err);
            step_done=true;
        }
    }

    return interval;
}

void Basic_render::init(SDL_Renderer *r)
{
    Screen::init(r);
    Sound::init(0);
    font.load(render);
    font.set_size(14);
    Console::print("Welcome to");
    Console::print("Mobile Basic 1.8.6 VM");
    Console::print("F5 - step; F6 - run program; F7 - reset");

    bas_process_timer = SDL_AddTimer(10, Basic_render::bas_process, nullptr);

}


void Basic_render::rendering(SDL_Renderer *r)
{

    if(console_mode)
        Console::draw();
    else
        Screen::draw(r);


    if(Screen::get_clear_flag()){
        Screen::set_clear_flag(false);
        Screen::clear();
        std_debug("Screen cleared");
    }

    BUSelect::draw(r);
    BUMessagebox::draw(r);
    BUEditform::draw(r);
}

void Basic_render::input(SDL_Event &e)
{
    if(e.type == SDL_KEYDOWN){
        if(e.key.keysym.sym==SDLK_F5){
            step_mode=true;
            step_done = false;
        }
        if(e.key.keysym.sym==SDLK_F6){
            step_mode = false;
        }
        if(e.key.keysym.sym==SDLK_F7){
            bas->reset();
            Console::print("resetted");
        }

        //basic input
        if(e.key.keysym.sym == SDLK_RETURN){
            Keymap::fire=true;
        }
        if(e.key.keysym.sym == SDLK_UP){
            Keymap::up=true;
        }
        if(e.key.keysym.sym == SDLK_RIGHT){
            Keymap::right=true;
        }
        if(e.key.keysym.sym == SDLK_LEFT){
            Keymap::left=true;
        }
        if(e.key.keysym.sym == SDLK_DOWN){
            Keymap::down=true;
        }

        //allkeys
        Keymap::register_key(e.key.keysym.sym);

        //numpad mapping
        if(e.key.keysym.sym == SDLK_KP_7){
            Keymap::register_key(49);
        }
        if(e.key.keysym.sym == SDLK_KP_8){
            Keymap::register_key(50);
            Keymap::up=true;
        }
        if(e.key.keysym.sym == SDLK_KP_9){
            Keymap::register_key(51);
        }
        if(e.key.keysym.sym == SDLK_KP_4){
            Keymap::register_key(52);
            Keymap::left=true;
        }
        if(e.key.keysym.sym == SDLK_KP_5){
            Keymap::register_key(53);
            Keymap::fire=true;
        }
        if(e.key.keysym.sym == SDLK_KP_6){
            Keymap::register_key(54);
            Keymap::right=true;
        }
        if(e.key.keysym.sym == SDLK_KP_1){
            Keymap::register_key(55);
        }
        if(e.key.keysym.sym == SDLK_KP_2){
            Keymap::register_key(56);
            Keymap::down=true;
        }
        if(e.key.keysym.sym == SDLK_KP_3){
            Keymap::register_key(57);
        }
        if(e.key.keysym.sym == SDLK_KP_0){
            Keymap::register_key(48);
        }

    }
    if(e.type == SDL_KEYUP){
        if(e.key.keysym.sym == SDLK_RETURN){
            Keymap::fire=false;
        }
        if(e.key.keysym.sym == SDLK_UP){
            Keymap::up=false;
        }
        if(e.key.keysym.sym == SDLK_RIGHT){
            Keymap::right=false;
        }
        if(e.key.keysym.sym == SDLK_LEFT){
            Keymap::left=false;
        }
        if(e.key.keysym.sym == SDLK_DOWN){
            Keymap::down=false;
        }

        //allkeys
        Keymap::unregister_key(e.key.keysym.sym);

        //numpad mapping
        if(e.key.keysym.sym == SDLK_KP_7){
            Keymap::unregister_key(49);
        }
        if(e.key.keysym.sym == SDLK_KP_8){
            Keymap::unregister_key(50);
            Keymap::up=false;
        }
        if(e.key.keysym.sym == SDLK_KP_9){
            Keymap::unregister_key(51);
        }
        if(e.key.keysym.sym == SDLK_KP_4){
            Keymap::unregister_key(52);
            Keymap::left=false;
        }
        if(e.key.keysym.sym == SDLK_KP_5){
            Keymap::unregister_key(53);
            Keymap::fire=false;
        }
        if(e.key.keysym.sym == SDLK_KP_6){
            Keymap::unregister_key(54);
            Keymap::right=false;
        }
        if(e.key.keysym.sym == SDLK_KP_1){
            Keymap::unregister_key(55);
        }
        if(e.key.keysym.sym == SDLK_KP_2){
            Keymap::unregister_key(56);
            Keymap::down=false;
        }
        if(e.key.keysym.sym == SDLK_KP_3){
            Keymap::unregister_key(57);
        }
        if(e.key.keysym.sym == SDLK_KP_0){
            Keymap::unregister_key(48);
        }
    }
    BUSelect::events(e);
    BUMessagebox::events(e);
    BUEditform::events(e);
}

void Basic_render::bas_call(string &filename)
{
    basStack.push_back(bas);
    bas = new BasFile();
    bas->load(filename);
}

void Basic_render::bas_endsub()
{
    delete bas;
    bas = basStack.back();
    basStack.pop_back();
}

BasFile *Basic_render::getCurrentBas()
{
    return bas;
}



