#include "fs.h"

#include "iostream"
#include "fstream"
#include <sys/stat.h>

FS::FS()
{

}

std::vector<char> FS::ReadAllBytes(string filename)
{
    ifstream ifs(filename, ios::binary|ios::ate);
    ifstream::pos_type pos = ifs.tellg();

    std::vector<char>  result(pos);

    ifs.seekg(0, ios::beg);
    ifs.read(&result[0], pos);

    return result;
}

string FS::DirName(string &filename)
{
    return filename.substr(0,filename.find_last_of("/")+1);
}

bool FS::file_exists(string &filename)
{
    struct stat buffer;
    return (stat (filename.c_str(), &buffer) == 0);
}
