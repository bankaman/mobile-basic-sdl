#include "texture.h"
#include "g.h"
#include "SDL2/SDL_image.h"

Texture::Texture()
{

}

void Texture::load(string filename, SDL_Renderer *r)
{
    render = r;
    SDL_Surface *s = IMG_Load(filename.c_str());
    if(s==nullptr){
        std_debug("error creating surface",filename,IMG_GetError());
        return;
    }
    sdl_texture = SDL_CreateTextureFromSurface( r, s );

    position.x=0;
    position.y=0;
    position.w=s->w;
    position.h=s->h;
    clip_rect.x=0;
    clip_rect.y=0;
    clip_rect.w=s->w;
    clip_rect.h=s->h;
    center.x=s->w/2;
    center.y=s->h/2;
    angle=0;
    flip = SDL_FLIP_NONE;

    SDL_FreeSurface(s);

    //std_debug("texture loaded",filename);
}

void Texture::draw(int x, int y)
{
    position.x=x;
    position.y=y;
    SDL_RenderCopyEx( render, sdl_texture, &clip_rect, &position, angle, &center, flip);
}

int Texture::get_w()
{
    return position.w;
}

int Texture::get_h()
{
    return position.h;
}
