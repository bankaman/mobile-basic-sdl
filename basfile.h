#ifndef BASFILE_H
#define BASFILE_H

#include <string>
#include <vector>
#include <memory>

using namespace std;

class BasFile
{
    struct VAR{
        int type;
        short unsigned int id;
        string name;
    };
    struct LINE{
        int num;
        vector<char> code;
    };

    struct FOR{
        int start_line;
        int varnum;
        int step;
        float endval;
    };

    struct PAUSE_STACK_EL{
        int line;
        int offset;
    };

public:
    BasFile();
    ~BasFile();
    bool load(string fname);
    void run(bool &err);
    bool step(bool &err);
    void reset();

    static int to_s_byte(int b);

private:
    static string opers[];
    vector<char> bytes;
    bool loaded;
    string current_file;
    string current_dir;
    int cursor;

    static int numeric_deep;

    float   numeric_expression(vector<char>&bytes,int &cursor, bool &err, int priority);
    float   numeric_expression(vector<char>&bytes,int &cursor, bool &err) {numeric_expression(bytes,cursor,err, 0);}
    string  string_expression(vector<char>&bytes,int &cursor, bool &err);
    bool    bool_expression(vector<char>&bytes,int &cursor, bool &err);

    int read_as_int4(vector<char> &bytes, int &cursor);
    int read_as_int2(vector<char> &bytes, int &cursor);
    int read_as_int2_r(vector<char> &bytes, int &cursor);
    unsigned char read_as_int1(vector<char> &bytes, int &cursor);
    string read_as_string(vector<char> &bytes, int &cursor);
    string read_as_string_1b(vector<char> &bytes, int &cursor);
    float read_as_float(vector<char> &bytes, int &cursor);
    int read_as_varnum(vector<char> &bytes, int &cursor,bool &err);
    int read_as_array_index(vector<char> &bytes, int &cursor,bool &err);

    string decompile_line(vector<char> &bytes);
    void clear();
    void clear_code();


    int find_line_id_by_num(int num);
    string try_get_oper(int id);

    bool expect_braket(vector<char> &bytes, int &cursor, bool &err);
    bool expect_braket_close(vector<char> &bytes, int &cursor, bool &err);
    bool expect_comma(vector<char> &bytes, int &cursor, bool &err);
    bool expect_equasion(vector<char> &bytes, int &cursor, bool &err);
    bool expect_sharp(vector<char> &bytes, int &cursor, bool &err);

    string  bas_readdir(string mask);
    int     bas_select(vector<char> &bytes, int &cursor, bool &err);
    int     bas_messagebox(vector<char> &bytes, int &cursor, bool &err);
    void    bas_setcolor(vector<char> &bytes, int &cursor, bool &err);
    void    bas_fillrect(vector<char> &bytes, int &cursor, bool &err);
    void    bas_drawstring(vector<char> &bytes, int &cursor, bool &err);
    void    bas_gelload(vector<char> &bytes, int &cursor, bool &err);
    void    bas_drawgel(vector<char> &bytes, int &cursor, bool &err);
    void    bas_spritegel(vector<char> &bytes, int &cursor, bool &err);
    void    bas_spritemove(vector<char> &bytes, int &cursor, bool &err);
    bool    bas_spritehit(vector<char> &bytes, int &cursor, bool &err);
    void    bas_for(vector<char> &bytes, int &cursor, bool &err);
    string  bas_left_s(vector<char> &bytes, int &cursor, bool &err);
    void    bas_next(vector<char> &bytes, int &cursor, bool &err);
    void    bas_setfont(vector<char> &bytes, int &cursor, bool &err);
    void    bas_open(vector<char> &bytes, int &cursor, bool &err);
    void    bas_inputf(vector<char> &bytes, int &cursor, bool &err);
    void    bas_dim(vector<char> &bytes, int &cursor, bool &err);
    void    bas_putf(vector<char> &bytes, int &cursor, bool &err);
    void    bas_getf(vector<char> &bytes, int &cursor, bool &err);
    void    bas_closef(vector<char> &bytes, int &cursor, bool &err);
    void    bas_printf(vector<char> &bytes, int &cursor, bool &err);
    void    bas_plot(vector<char> &bytes, int &cursor, bool &err);
    int     bas_days(vector<char> &bytes, int &cursor, bool &err);
    int     bas_milliseconds(vector<char> &bytes, int &cursor, bool &err);
    int     bas_second(vector<char> &bytes, int &cursor, bool &err);
    void    bas_playwav(vector<char> &bytes, int &cursor, bool &err);
    void    bas_delete(vector<char> &bytes, int &cursor, bool &err);
    int     bas_editform(vector<char> &bytes, int &cursor, bool &err);


    vector<int> int_vars;
    vector<float> float_vars;
    vector<string> string_vars;

    vector<shared_ptr<vector<int>>> int_arrays;
    vector<shared_ptr<vector<float>>> float_arrays;
    vector<shared_ptr<vector<string>>> string_arrays;

    vector<VAR*> vars;
    vector<LINE*> lines;

    vector<shared_ptr<FOR>> fors;

    //vector<int> gosub_stack;
    vector<PAUSE_STACK_EL*> pause_stack;

    int cur_line;
    int next_line;
    int trap_line;
    bool is_over;


};

#endif // BASFILE_H

