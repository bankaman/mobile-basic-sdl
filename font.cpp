#include "font.h"
#include <iostream>
#include "g.h"
#include <fs.h>

Font::Font()
{
    sdl_texture = nullptr;
    size = 20;
    textColor = { 0, 0, 0 };
    loaded=false;
    font=nullptr;
    h=-1;
    w=-1;
}

Font::~Font()
{
    destroy_font();
}

bool Font::load(SDL_Renderer *r)
{
    destroy_font();
    render = r;
    string path = G::binary_dir+"font/UbuntuMono-R.ttf";
    if(!FS::file_exists(path)){
        std_debug("file not found",path);
        return false;
    }
    font = TTF_OpenFont( path.c_str() , size );

    TTF_SizeUTF8(font, "T", &w, &h);

    if(font==nullptr){
        std_debug("error loading font, ",TTF_GetError());
        return false;
    }
    loaded=true;
    return true;
}

void Font::drawText(std::string &str, int x, int y)
{
    if(!loaded)
        return;
    if(str.length()==0)
        return;
    if(sdl_texture!=nullptr)
        SDL_DestroyTexture( sdl_texture );

    SDL_Surface* textSurface = nullptr;

    string t=str;

    textSurface = TTF_RenderUTF8_Blended( font, t.c_str(), textColor );

    if(textSurface==nullptr){
        std_debug("unable to create surface for font");
        return ;
    }
    sdl_texture = SDL_CreateTextureFromSurface( render, textSurface);
    if(sdl_texture==nullptr){
        std_debug("unable to create texture for font");
        return;
    }

    position.x = x;
    position.y = y;
    int w,h;
    SDL_QueryTexture(sdl_texture,nullptr,nullptr,&w,&h);
    position.w = w;
    position.h = h;
    clip_rect.x=0;
    clip_rect.y=0;
    clip_rect.w=position.w;
    clip_rect.h=position.h;
    center.x = position.w/2;
    center.y = position.h/2;

    SDL_RenderCopyEx( render, sdl_texture,&clip_rect,&position,0,&center,SDL_FLIP_NONE);

    SDL_FreeSurface( textSurface );
    textSurface=nullptr;

}

int Font::get_height()
{
    return h;
}

int Font::get_width()
{
    return w;
}

int Font::get_text_width(string &str)
{
    if(!loaded)
        return -1;
    int mw=0;

    TTF_SizeUTF8(font, str.c_str(), &mw, &h);

    return  mw;
}

void Font::set_color(SDL_Color color)
{
    old_textColor = textColor;
    textColor = color;
}

void Font::set_size(int _size)
{
    size=_size;
    load(render);
}

void Font::return_color()
{
    textColor = old_textColor;
}

void Font::destroy_font()
{
    if(font==nullptr)
        return;
    //TTF_CloseFont(font);
    font=nullptr;
}
