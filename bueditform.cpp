#include "bueditform.h"


#include "font.h"
#include "basic_render.h"
#include "g.h"

string BUEditform::title;
string BUEditform::sub_title;

string BUEditform::ok_btn;
string BUEditform::cancel_btn;
bool BUEditform::selected;
bool BUEditform::active=false;
int BUEditform::result=1;
string BUEditform::text;


BUEditform::BUEditform()
{

}

void BUEditform::show(string &_title, string &_sub_title, string &_var_content, string &_ok_btn, string &_cancel_btn)
{
    title = _title;
    sub_title = _sub_title;
    text = _var_content;
    ok_btn = _ok_btn;
    cancel_btn = _cancel_btn;

    active=true;
    SDL_StartTextInput();
}

bool BUEditform::is_selected()
{
    return selected;
}

int BUEditform::get_result()
{
    return result;
}

string BUEditform::get_text()
{
    return text;
}

void BUEditform::draw(SDL_Renderer *r)
{
    if(!active)
        return;

    SDL_Rect rect;
    rect.x=10;
    rect.y=10;
    rect.w=50;
    rect.h=50;
    Uint8 red,green,blue,alpha;

    Font* font = &Basic_render::font;

    rect.w = font->get_text_width(text)+10;
    if(rect.w < 300)
        rect.w = 300;
    rect.h = font->get_height()*6;
    rect.x = G::scr_w/2 - rect.w/2;
    rect.y = G::scr_h/2 - rect.h/2;

    SDL_RenderFillRect(r,&rect);
    SDL_GetRenderDrawColor(r,&red,&green,&blue,&alpha);
    SDL_SetRenderDrawColor(r,0,0,0,0xff);
    SDL_RenderDrawRect(r,&rect);

    font->drawText(title,rect.x+rect.w/2-font->get_text_width(title)/2,rect.y+5);
    font->drawText(sub_title,rect.x+rect.w/2-font->get_text_width(sub_title)/2,rect.y+5+font->get_height());
    font->drawText(text,rect.x+rect.w/2-font->get_text_width(text)/2,rect.y+10+font->get_height()*2);
    SDL_Rect sel;
    if(result==-1){
        font->set_color({0,0,0});
        sel.x=rect.x;
        sel.w=rect.w/2;
        sel.y=rect.y+rect.h-font->get_height()-5;
        sel.h=font->get_height()+5;
        SDL_RenderFillRect(r,&sel);
        font->set_color({0xff,0xff,0xff});
    }

    font->drawText(cancel_btn,
                       rect.x+rect.w/4-font->get_text_width(cancel_btn)/2,
                       rect.y+rect.h-font->get_height()-5
                   );

    if(result==-1){
        font->return_color();
    }

    if(result==1){
        font->set_color({0,0,0});
        sel.x=rect.x+rect.w/2;
        sel.w=rect.w/2;
        sel.y=rect.y+rect.h-font->get_height()-5;
        sel.h=font->get_height()+5;
        SDL_RenderFillRect(r,&sel);
        font->set_color({0xff,0xff,0xff});
    }

    font->drawText(ok_btn,
                       rect.x+rect.w/2+rect.w/4-font->get_text_width(ok_btn)/2,
                       rect.y+rect.h-font->get_height()-5
                   );

    if(result==1){
        font->return_color();
    }


    SDL_SetRenderDrawColor(r,red,green,blue,alpha);
}

void BUEditform::events(SDL_Event &e)
{
    if(!active)
        return;
    if(e.type == SDL_KEYDOWN){
        if(e.key.keysym.sym==SDLK_LEFT){
            result=-1;
        }
        if(e.key.keysym.sym==SDLK_RIGHT){
            result=1;
        }
        if(e.key.keysym.sym==SDLK_RETURN){
            selected=true;
            active=false;
            SDL_StopTextInput();
        }
        if(e.key.keysym.sym==SDLK_BACKSPACE){
            if(text.length()>0)
                text = Charset::utf8_substr(text,0,Charset::utf8_strlen(text)-1);
        }
    }
    if(e.type == SDL_TEXTINPUT){
        text.append(e.text.text);
    }
    if(e.type == SDL_TEXTINPUT){
        //std_debug("edit!",e.edit.text);
    }
}
