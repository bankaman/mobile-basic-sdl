#ifndef G_H
#define G_H

#include <iostream>
using namespace std;

void std_debug();// { cerr << endl; }
template <typename Head, typename... Tail> void std_debug(Head H, Tail... T) {
  cerr << H << ' ';
  std_debug(T...);
}




class G
{
public:
    G();

    static int scr_w;
    static int scr_h;

    static string binary_dir;


};

#endif // G_H
