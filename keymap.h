#ifndef KEYMAP_H
#define KEYMAP_H

#include <vector>

using namespace std;

class Keymap
{
public:
    Keymap();

    static bool fire;
    static bool up;
    static bool right;
    static bool left;
    static bool down;

    static void register_key(int code);
    static void unregister_key(int code);

    static int bas_inkey();
private:

    static int last_inkey_id;

    static int get_id_by_code(int code);

    static vector<int> keys;
};

#endif // KEYMAP_H
