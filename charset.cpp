#include "charset.h"

Charset::Charset()
{

}

string Charset::to_utf8(string str)
{
    string out;
    for(int i=0;i<str.length();i++){
        unsigned char ttt=str.at(i);
        if((ttt>=0xC0)&&(ttt<=0xEF)) {
            out.push_back(0xD0);
            out.push_back(ttt-48);
        }else if((ttt>=0xF0)&&(ttt<=0xFF)) {
            out.push_back(0xD1);
            out.push_back(ttt-112);
        }else{

            switch (ttt){
                case 0xA8://Ё
                    out.push_back(0xD0);
                    out.push_back(0x81);
                break;
                case 0xB8://ё
                    out.push_back(0xD1);
                    out.push_back(0x91);
                break;
                default:
                    out.push_back(ttt);
                break;
            }
        }
    }
    return out;
}

int Charset::utf8_strlen(string& str)
{
    int c,i,ix,q;
    for (q=0, i=0, ix=str.length(); i < ix; i++, q++)
    {
        c = (unsigned char) str[i];
        if      (c>=0   && c<=127) i+=0;
        else if ((c & 0xE0) == 0xC0) i+=1;
        else if ((c & 0xF0) == 0xE0) i+=2;
        else if ((c & 0xF8) == 0xF0) i+=3;
        //else if (($c & 0xFC) == 0xF8) i+=4; // 111110bb //byte 5, unnecessary in 4 byte UTF-8
        //else if (($c & 0xFE) == 0xFC) i+=5; // 1111110b //byte 6, unnecessary in 4 byte UTF-8
        else return 0;//invalid utf8
    }
    return q;
}

string Charset::utf8_substr(const string& str, unsigned int start, unsigned int leng)
{
    if (leng==0) { return ""; }
    unsigned int c, i, ix, q, min=std::string::npos, max=std::string::npos;
    for (q=0, i=0, ix=str.length(); i < ix; i++, q++)
    {
        if (q==start){ min=i; }
        if (q<=start+leng || leng==std::string::npos){ max=i; }

        c = (unsigned char) str[i];
        if      (
                 //c>=0   &&
                 c<=127) i+=0;
        else if ((c & 0xE0) == 0xC0) i+=1;
        else if ((c & 0xF0) == 0xE0) i+=2;
        else if ((c & 0xF8) == 0xF0) i+=3;
        //else if (($c & 0xFC) == 0xF8) i+=4; // 111110bb //byte 5, unnecessary in 4 byte UTF-8
        //else if (($c & 0xFE) == 0xFC) i+=5; // 1111110b //byte 6, unnecessary in 4 byte UTF-8
        else return "";//invalid utf8
    }
    if (q<=start+leng || leng==std::string::npos){ max=i; }
    if (min==std::string::npos || max==std::string::npos) { return ""; }
    return str.substr(min,max);
}
