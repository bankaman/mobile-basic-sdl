#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>

#include <SDL2/SDL.h>

using namespace std;

class Texture
{
public:
    Texture();

    void load(string filename, SDL_Renderer *r);
    void draw(int x, int y);
    int get_w();
    int get_h();

private:
    SDL_Texture* sdl_texture;
    SDL_Renderer* render;

    SDL_Rect clip_rect;
    SDL_Rect position;
    SDL_Point center;
    SDL_RendererFlip flip;
    int angle;

};

#endif // TEXTURE_H
