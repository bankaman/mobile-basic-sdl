
#include <iostream>

#include "files.h"
#include "fs.h"
#include "g.h"
#include "basfile.h"

map<int,shared_ptr<fstream>> Files::channels;

Files::Files()
{

}

void Files::open_channel(int channel, string &file_name, string &mode, bool &err)
{


    if(channels.find(channel)!=channels.end()){
        err=true;
        std_debug("channel already in use!",channel);
        return;
    }

    shared_ptr<fstream> file(new fstream());
    if(mode=="input"){

        if(!FS::file_exists(file_name)){
            std_debug("file not found!",file_name);
            err=true;
            return;
        }

        file->open(file_name, ios::binary | ios::in);
    }
    if(mode=="output"){
        file->open(file_name, ios::binary | ios::out);
    }

    if(!file->is_open()){
        err=true;
        std_debug("error opening file!",file_name);
        return;
    }


    channels.insert({channel,file});
}

void Files::close_channel(int channel, bool &err)
{
    std_debug("channel closing",channel);
    if(channels.find(channel)==channels.end()){
        err=true;
        std_debug("channel not opened!",channel);
        return;
    }

    channels.at(channel)->close();
    channels.erase(channel);
}

int Files::input_int(int channel, bool &err)
{
    shared_ptr<fstream> file;

    if(channels.find(channel)==channels.end()){
        err=true;
        std_debug("channel not opened!",channel);
        return 0;
    }

    file = channels.at(channel);

    int result=0;
    char b;

    try {
        result = file->get()<<24;
        result |= file->get()<<16;
        result |= file->get()<<8;
        result |= file->get();
    }catch (std::ios_base::failure& e) {
        std_debug(e.what());
        err=true;
        return 0;
    }

    return result;
}

float Files::input_float(int channel, bool &err)
{
    shared_ptr<fstream> file;

    if(channels.find(channel)==channels.end()){
        err=true;
        std_debug("channel not opened!",channel);
        return 0;
    }

    file = channels.at(channel);

    double result;
    int base;
    int exp;
    try {
        base = (unsigned char)file->get()<<16;
        base |= (unsigned char)file->get()<<8;
        base |= (unsigned char)file->get();

        exp = file->get();
        if(base & 0b100000000000000000000000){
            base &= 0b011111111111111111111111;
            base *=-1;
        }
        std_debug("base:",base,"exp:",exp);
    }catch (std::ios_base::failure& e) {
        std_debug(e.what());
        err=true;
        return 0;
    }



    double m = base/500000.0;


    //std_debug("base:",base,"exp:",exp);

    double e=1;
    double d;
    if(exp>0){
        d=1;
        for(int i=0;i<exp;i++)
            d*=10;
        e=d;
    }
    if(exp<0){
        d=1;
        for(int i=exp;i<0;i++)
            d/=10;
        e=d;
    }
    result = m*e;

    return result;

}

string Files::input_string(int channel, bool &err)
{
    string result = "";
    shared_ptr<fstream> file;

    if(channels.find(channel)==channels.end()){
        err=true;
        std_debug("channel not opened!",channel);
        return result;
    }

    file = channels.at(channel);

    int len = 0;

    len = file->get() << 8;
    len |= file->get();

    for(int i = 0;i<len;i++){
        result += (char)file->get();
    }

    return result;
}

void Files::print_int(int channel, int val, bool &err)
{
    shared_ptr<fstream> file;

    if(channels.find(channel)==channels.end()){
        err=true;
        std_debug("channel not opened!",channel);
        return;
    }

    file = channels.at(channel);
    try {

        file->put(val>>24&0xff);
        file->put(val>>16&0xff);
        file->put(val>>8&0xff);
        file->put(val&0xff);

    }catch (std::ios_base::failure& e) {
        std_debug(e.what());
        err=true;
        return;
    }
}

void Files::print_float(int channel, float val, bool &err)
{
    shared_ptr<fstream> file;

    if(channels.find(channel)==channels.end()){
        err=true;
        std_debug("channel not opened!",channel);
        return;
    }
    file = channels.at(channel);

    std_debug("writing",val);

    int exp = 0;


    double num = val;
    if (abs(num) < 1) {
        if(num==0)
            exp=0;
        while (abs(num) < 1 && num!=0) {
            num = num * 10;
            exp--;
            std_debug(num);
        }
    } else if (abs(num) >= 10) {
        while (abs(num) >= 10) {
            num = num / 10;
            exp++;
        }
    }
    int radix = (abs(num) * 500000);
    if(num<0)
        radix |= 0b100000000000000000000000;
    std_debug("radix:",radix,"num:",num,"exp:",exp);

    try {
        //std_debug("writting,",hex,radix,hex,exp);

        file->put(radix>>16&0xff);
        file->put(radix>>8&0xff);
        file->put(radix&0xff);
        file->put(exp&0xff);

        std_debug("written,",hex,radix,hex,exp);

    }catch (std::ios_base::failure& e) {
        std_debug(e.what());
        err=true;
        return;
    }


}
void Files::print_string(int channel, string str, bool &err)
{
    shared_ptr<fstream> file;

    if(channels.find(channel)==channels.end()){
        err=true;
        std_debug("channel not opened!",channel);
        return;
    }

    file = channels.at(channel);
    try {

        file->put(str.length()>>8&0xff);
        file->put(str.length()&0xff);

        for(int i = 0;i<str.length();i++){
            file->put(str.at(i));
        }

    }catch (std::ios_base::failure& e) {
        std_debug(e.what());
        err=true;
        return;
    }
}

int Files::get_byte(int channel, bool &err)
{
    shared_ptr<fstream> file;

    if(channels.find(channel)==channels.end()){
        err=true;
        std_debug("channel not opened!",channel);
        return 0;
    }

    file = channels.at(channel);

    int result=0;
    char b;

    try {
        if(file->eof()){
            err=true;
            return 0;
        }
        result = file->get();

    }catch (std::ios_base::failure& e) {
        std_debug(e.what());
        err=true;
        return 0;
    }
    return result;
}

void Files::put_byte(int channel, int byte, bool &err)
{
    shared_ptr<fstream> file;

    if(channels.find(channel)==channels.end()){
        err=true;
        std_debug("channel not opened!",channel);
        return;
    }

    file = channels.at(channel);
    file->put(byte && 0xFF);
}

void Files::close_all()
{
    for(auto &e : channels){
        if(e.second->is_open())
            e.second->close();
    }
}
