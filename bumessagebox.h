#ifndef BUMESSAGEBOX_H
#define BUMESSAGEBOX_H

#include <string>
#include <SDL2/SDL.h>
using namespace std;

class BUMessagebox
{
public:
    BUMessagebox();

    static void show(string &_title, string &_sub_title, string &_message, string &_ok_btn, string &_cancel_btn);
    static bool is_selected();
    static int get_result();

    static void draw(SDL_Renderer* r);
    static void events(SDL_Event &e);

private:
    static string title;
    static string sub_title;
    static string message;
    static string ok_btn;
    static string cancel_btn;
    static bool selected;
    static bool active;
    static int result;

};

#endif // BUMESSAGEBOX_H
