#ifndef FS_H
#define FS_H

#include <vector>
#include <string>

using namespace std;

class FS
{
public:
    FS();
    static std::vector<char> ReadAllBytes(string filename);
    static string DirName(string &filename);
    static bool file_exists(string &filename);
};

#endif // FS_H
