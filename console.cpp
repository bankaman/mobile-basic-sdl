#include "console.h"
#include "basic_render.h"
#include "g.h"

vector<string> Console::console;

Console::Console()
{

}

void Console::draw()
{
    int h = Basic_render::font.get_height();
    int w;
    string base_str;
    string temp1;
    string temp2;
    int l=0;
    int cut;
    for(size_t i=0;i<console.size();i++){
        base_str = console.at(i);
        temp1=base_str;
        temp2.clear();
        cut=0;
        do{
            w = Basic_render::font.get_text_width(temp1);
            if(w<0) break;
            if(w>G::scr_w-10){
                cut++;
                temp2 = base_str.substr(base_str.length()-cut,cut);
                temp1 = base_str.substr(0,base_str.length()-cut);
            }else{
                Basic_render::font.drawText(temp1,10,10+l*h);
                l++;
                if(cut>0){
                    temp1 = temp2;
                    base_str = temp2;
                    temp2.clear();
                    cut=0;
                    w=-1;
                }
            }
        }while((w>G::scr_w-10)||(w==-1));



    }
}

void Console::print(string str)
{
    console.push_back(str);
    while(console.size()>(G::scr_h-10)/Basic_render::font.get_height()){
        console.erase(console.begin());
    }
    Basic_render::console_mode=true;
}

void Console::clear()
{
    console.clear();
}

std::vector<std::string> Console::explode(std::string const & s, char delim)
{
    std::vector<std::string> result;
    string temp;

    int counter=0;
    temp.clear();
    for(size_t i=0; i<s.length();i++){
        if(s.at(i)==delim) {
            result.push_back(temp);
            temp.clear();
            counter=0;
            continue;
        }

        temp.push_back(s.at(i));
        counter++;
    }

    return result;
}
